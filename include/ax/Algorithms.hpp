//
// Created by Martin Miralles-Cordal on 8/3/19.
//

#pragma once

#include "TypeTraits.hpp"

namespace ax {

#define AX_UNWRAP_RANGE(container) std::begin(container), std::end(container)

template<size_t Reps, typename Func>
void repeat(Func f) { for (size_t i = 0; i < Reps; ++i) f(); }

template<size_t Reps, typename Func, typename OutputIt, typename = std::enable_if_t<!returns_void<Func>>>
OutputIt repeat(Func f, OutputIt out) { repeat<Reps>([&] { *out++ = f(); }); return out; }

template <typename InputRange, typename OutputIt, typename Func>
auto transform(InputRange input, OutputIt output, Func func)
{
    return std::transform(AX_UNWRAP_RANGE(input), output, func);
}

template <typename InputRange, typename T>
auto find(InputRange input, T const &value) { return std::find(AX_UNWRAP_RANGE(input), value); }

template <typename InputRange, typename Func>
auto find_if(InputRange input, Func func) { return std::find_if(AX_UNWRAP_RANGE(input), func); }

template<typename F, typename Tuple, std::size_t ...S >
constexpr decltype(auto) _apply_impl(F&& fn, Tuple&& t, std::index_sequence<S...>)
{
    return std::forward<F>(fn)(std::get<S>(std::forward<Tuple>(t))...);
}

template<typename F, typename Tuple>
constexpr decltype(auto) apply(F&& fn, Tuple&& t)
{
    constexpr auto tSize = std::tuple_size<typename std::remove_reference<Tuple>::type>::value;
    return _apply_impl(std::forward<F>(fn), std::forward<Tuple>(t), std::make_index_sequence<tSize>());
}

/**
 * Performs a left fold over the range, applying a combining operation to each element and the running result.
 * @tparam InputIt Input iterator type.
 * @tparam T Accumulator type.
 * @tparam BinaryOp The function to apply. Takes the accumulator and a range element in that order.
 * @param first The start point.
 * @param last The end point.
 * @param init The initial value for the accumulator.
 * @param op The function to apply.
 * @return The accumulated value.
 */
template<typename InputIt, typename T, typename BinaryOp>
constexpr T foldl(InputIt first, InputIt last, T init, BinaryOp op)
{
    for (; first != last; ++first) { init = op(std::move(init), *first); }
    return init;
}

/**
 * Performs a left fold over the container, applying a combining operation to each element and the running result.
 * @tparam Container The container type.
 * @tparam T Accumulator type.
 * @tparam BinaryOp The function to apply. Takes the accumulator and a range element in that order.
 * @param c The container to iterate over.
 * @param init The initial value for the accumulator.
 * @param op The function to apply.
 * @return The accumulated value.
 */
template <typename Container, typename T, typename BinaryOp>
constexpr T foldl(Container c, T init, BinaryOp op)
{
    return foldl(AX_UNWRAP_RANGE(c), std::move(init), std::move(op));
}

/**
 * Performs a right fold over the range, applying a combining operation to each element and the running result.
 * @tparam InputIt Input iterator type.
 * @tparam T Accumulator type.
 * @tparam BinaryOp The function to apply. Takes the accumulator and a range element in that order.
 * @param first The start point.
 * @param last The end point.
 * @param init The initial value for the accumulator.
 * @param op The function to apply.
 * @return The accumulated value.
 */
template<typename InputIt, typename T, typename BinaryOp>
constexpr auto foldr(InputIt first, InputIt last, T init, BinaryOp op)
{
    return foldl(std::make_reverse_iterator(last), std::make_reverse_iterator(first), init, op);
}

/**
 * Performs a right fold over the container, applying a combining operation to each element and the running result.
 *
 * @tparam Container The container type.
 * @tparam T Accumulator type.
 * @tparam BinaryOp The function to apply. Takes the accumulator and a range element in that order.
 * @param c The container to iterate over.
 * @param init The initial value for the accumulator.
 * @param op The function to apply.
 * @return The accumulated value.
 */
template<typename Container, typename T, typename BinaryOp>
constexpr auto foldr(Container c, T init, BinaryOp op) { return foldl(std::rbegin(c), std::rend(c), init, op); }

} // namespace ax
