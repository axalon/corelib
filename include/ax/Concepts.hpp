//
// Created by Martin Miralles-Cordal on 8/27/19.
//

#ifndef CORELIB_INCLUDE_AX_CONCEPTS_HPP
#define CORELIB_INCLUDE_AX_CONCEPTS_HPP

#include <ax/TypeTraits.hpp>

#define CONCEPT constexpr bool
namespace ax {

template <bool B> using is = bool_constant<B>;
template <bool... Bs> constexpr bool require = all<is<Bs>...>::value;
template <bool... Bs> constexpr bool either = any<is<Bs>...>::value;
template <bool... Bs> constexpr bool disallow = !require<Bs...>;

template <template <class...> class Op, class... Args> constexpr bool exists = is_detected<Op, Args...>::value;
template <template <class...> class Op, class... Args> constexpr bool supports = exists<Op, Args...>;
template <typename To, template <class...> class Op, class... Args> constexpr bool converts_to = is_detected_convertible<To, Op, Args...>::value;
template <class Exact, template <class...> class Op, class... Args> constexpr bool identical_to = is_detected_exact<Exact, Op, Args...>::value;

template <bool...Bs> using if_conforming = std::enable_if_t<require<Bs...>>;

namespace adl {
using std::swap;
template<typename T, class U=T> using swap_with = decltype(swap(std::declval<T>(), std::declval<U>()));
}

namespace alias {
template <typename T> using value_type        = typename T::value_type;
template <typename T> using reference         = typename T::reference;
template <typename T> using iterator_category = typename T::iterator_category;
template <typename T> using pointer           = typename T::pointer;
}

namespace ops {
using std::declval;
template <typename T> std::add_lvalue_reference_t<T> declref();

template <typename T, typename U = T> using less          = decltype(declval<T>() < declval<U>());
template <typename T, typename U = T> using less_equal    = decltype(declval<T>() <= declval<U>());
template <typename T, typename U = T> using equal         = decltype(declval<T>() == declval<U>());
template <typename T, typename U = T> using greater_equal = decltype(declval<T>() >= declval<U>());
template <typename T, typename U = T> using greater       = decltype(declval<T>() > declval<U>());
template <typename T, typename U = T> using not_equal     = decltype(declval<T>() != declval<U>());

template <typename T, typename U> using at        = decltype(declval<T>()[declval<U>()]);
template <typename T, typename U> using subscript = at<T, U>;

template <typename T> using dereference       = decltype(*declval<T>());
template <typename T> using arrow             = decltype(declval<T>().operator->());
template <typename T> using postfix_increment = decltype(declref<T>()++);
template <typename T> using prefix_increment  = decltype(++declref<T>());
}

namespace stl {
template <typename T> using std_begin = decltype(std::begin(std::declval<T>()));
template <typename T> using std_end = decltype(std::end(std::declval<T>()));
}

inline namespace concept {

#define GENERATE_MEMBER_TRAIT(member) namespace member { template <typename T> using member = decltype(std::declval<T>().member); }
#define GENERATE_METHOD_TRAIT(member) namespace method { \
template <typename T, typename...Args> using member = decltype(std::declval<T>().member(std::declval<Args>()...)); }

#define GENERATE_STATIC_VARIABLE_TRAIT(variable) namespace static_member { template <typename T> using variable = decltype(T::variable); }
#define GENERATE_STATIC_FUNCTION_TRAIT(function) namespace static_function { \
template <typename T, typename...Args> using function = decltype(T::function(std::declval<Args>()...)); }

template <typename T, class U> constexpr bool SwappableWith = exists<adl::swap_with, T, U>;
template <typename T> constexpr bool Swappable = SwappableWith<T&, T&>;
template <typename T, typename U> constexpr bool SameAs = std::is_same<T, U>::value;
template <typename T, typename ...Ts> constexpr bool MatchesAny = is_any_of<T, Ts...>::value;

template <typename T, typename...Args> constexpr bool Constructible = std::is_constructible<T, Args...>::value;
template <typename T, typename U> constexpr bool Assignable = std::is_assignable<T, U>::value;
template <typename T> constexpr bool DefaultConstructible = std::is_default_constructible<T>::value;
template <typename T> constexpr bool CopyConstructible = std::is_copy_constructible<T>::value;
template <typename T> constexpr bool CopyAssignable = std::is_copy_assignable<T>::value;
template <typename T> constexpr bool CopyMutable = CopyConstructible<T> && CopyAssignable<T>;
template <typename T> constexpr bool MoveConstructible = std::is_move_constructible<T>::value;
template <typename T> constexpr bool MoveAssignable = std::is_move_assignable<T>::value;
template <typename T> constexpr bool MoveMutable = MoveConstructible<T> && MoveAssignable<T>;
template <typename T> constexpr bool Destructible = std::is_destructible<T>::value;

template <typename T, typename...Args> constexpr bool TriviallyConstructible = std::is_trivially_constructible<T, Args...>::value;
template <typename T, typename U> constexpr bool TriviallyAssignable = std::is_trivially_assignable<T, U>::value;
template <typename T> constexpr bool TriviallyDefaultConstructible = std::is_trivially_default_constructible<T>::value;
template <typename T> constexpr bool TriviallyCopyConstructible = std::is_trivially_copy_constructible<T>::value;
template <typename T> constexpr bool TriviallyCopyAssignable = std::is_trivially_copy_assignable<T>::value;
template <typename T> constexpr bool TriviallyCopyable = std::is_trivially_copyable<T>::value;
template <typename T> constexpr bool TriviallyMoveConstructible = std::is_trivially_move_constructible<T>::value;
template <typename T> constexpr bool TriviallyMoveAssignable = std::is_trivially_move_assignable<T>::value;
template <typename T> constexpr bool TriviallyDestructible = std::is_trivially_destructible<T>::value;

template <typename T, typename...Args> constexpr bool NothrowConstructible = std::is_nothrow_constructible<T, Args...>::value;
template <typename T, typename U> constexpr bool NothrowAssignable = std::is_nothrow_assignable<T, U>::value;
template <typename T> constexpr bool NothrowDefaultConstructible = std::is_nothrow_default_constructible<T>::value;
template <typename T> constexpr bool NothrowCopyConstructible = std::is_nothrow_copy_constructible<T>::value;
template <typename T> constexpr bool NothrowCopyAssignable = std::is_nothrow_copy_assignable<T>::value;
template <typename T> constexpr bool NothrowMoveConstructible = std::is_nothrow_move_constructible<T>::value;
template <typename T> constexpr bool NothrowMoveAssignable = std::is_nothrow_move_assignable<T>::value;
template <typename T> constexpr bool NothrowDestructible = std::is_nothrow_destructible<T>::value;

template <typename T> constexpr bool Pointer = std::is_pointer<T>::value;
template <typename T, class U=T> constexpr bool EqualityComparable = converts_to<bool, ops::equal, T, U>;

template <typename T> constexpr bool Hashable = is_std_hashable<T>::value;

template <typename T>
constexpr bool Iterator = require<CopyConstructible<T>,
                                  CopyAssignable<T>,
                                  Destructible<T>,
                                  Swappable<T>,
                                  exists<ops::postfix_increment, T>,
                                  exists<ops::prefix_increment, T>,
                                  exists<ops::dereference, T>>;

template <typename T>
constexpr bool InputIterator = either<Pointer<T>,
                               require<EqualityComparable<T>,
                                       Iterator<T>,
                                       exists<alias::value_type, T>,
                                       exists<alias::reference, T>,
                                       either<identical_to<detected_t<alias::reference, T>, ops::dereference, T>,
                                              converts_to<detected_t<alias::value_type, T>, ops::dereference, T>>,
                                       identical_to<detected_t<alias::pointer, T>, ops::arrow, T>,
                                       converts_to<detected_t<alias::value_type, T>, ops::dereference, T&>,
                                       converts_to<std::input_iterator_tag, alias::iterator_category, T>>>;

} // namespace concept

} // namespace ax

#endif //CORELIB_INCLUDE_AX_CONCEPTS_HPP
