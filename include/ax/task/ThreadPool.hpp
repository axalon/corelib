//
// Created by Martin Miralles-Cordal on 8/12/19.
//

#pragma once

#include <ax/task/Executor.hpp>
#include <ax/task/PunchClock.hpp>
#include <ax/task/Queue.hpp>

namespace ax {
inline namespace task {

template <uint32_t ThreadCount = 1u>
class ThreadPool
{
  public:
    static_assert(ThreadCount > 0, "Must have a positive number of threads!");
    ThreadPool() { start(); }
    explicit ThreadPool(DeferredStartTag) {}

    ThreadPool(ThreadPool const &) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;

    ThreadPool(ThreadPool &&rhs)
    {
        move_(std::move(rhs));
        // Our queue is now in the right state, but our threads are not.
        // If rhs was valid (i.e. paused or running), we should be too, so we need to spin up our threads.
        if (queue_.valid()) { spinUpThreads(); }
    }

    ThreadPool &operator=(ThreadPool &&rhs)
    {
        if (running()) { shutdown(); }
        Move_(std::move(rhs));
        // Our queue is now in the right state, but our threads are not.
        // If rhs was valid (i.e. paused or running), we should be too, so we need to spin up our threads.
        if (queue_.valid()) { spinUpThreads(); }
        return *this;
    }

    ~ThreadPool() { shutdown(); }

    void dispatch(fu2::function<void() const> f) { schedule(std::move(f)); }
    void schedule(fu2::function<void() const> f) { push(std::move(f)); }

    void start()
    {
        if (queue_.resume()) { spinUpThreads(); }
    }

    void shutdown()
    {
        using namespace std::literals;
        if (queue_.invalidate())
        {
            while(!punchClock_.waitUntilEmptyFor(50ms)) { queue_.notifyAll(); }
            for (auto &thread : threads_) { if (thread.joinable()) { thread.join(); } }
        }
    }

    bool pause() { return queue_.pause(); }
    bool resume() { return queue_.resume(); }

    [[nodiscard]] bool valid() const { return queue_.valid(); }
    [[nodiscard]] bool invalid() const { return queue_.invalid(); }
    [[nodiscard]] bool paused() const { return queue_.paused(); }
    [[nodiscard]] bool running() const { return queue_.running(); }

    void push(fu2::function<void() const> f)
    {
        queue_.push(std::move(f));
    }

    [[nodiscard]] bool hasJobs() const { return queue_.hasJobs(); }

    template<typename F, typename...Args>
    auto dispatch(F&& f, Args&&... args) -> std::future<std::invoke_result_t<F, Args...>>
    {
        return queue_.dispatch(std::forward<F>(f), std::forward<Args>(args)...);
    }

  private:
    void move_(ThreadPool &&rhs)
    {
        // grab state before we shut down rhs
        auto state = rhs.queue_.state();

        rhs.pause();
        queue_ = std::move(rhs.queue_);
        rhs.shutdown();

        // restore old rhs queue state
        queue_.overrideState(state);
    }

    void spinUpThreads()
    {
        for (unsigned int i = 0u; i < ThreadCount; ++i) { threads_[i] = std::thread{Queue::Worker{i, queue_, punchClock_}}; }
    }

    std::array<std::thread, ThreadCount> threads_;
    Queue queue_;
    PunchClock punchClock_;
};

}} // namespace ax::task
