//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <utility>

namespace ax {
inline namespace task {

class EventListener { virtual ~EventListener() = default; };

class Event
{
  public:
    virtual ~Event() = default;
    virtual void inform(EventListener &listener) const = 0;
};

template <typename E>
class IEventListener : public EventListener
{
  public:
    virtual void onEvent(E const &event) = 0;
};

template <typename Derived>
class BaseEvent : public Event
{
  public:
    void inform(IEventListener<Derived> &listener) const { listener.onEvent(static_cast<const Derived&>(*this)); }
    void inform(EventListener &listener) const override
    {
        if (auto *derivedListener = dynamic_cast<IEventListener<Derived>*>(&listener)) { inform(*derivedListener); }
    }
};

template <typename E>
class EventCast : public Event
{
  public:
    EventCast(E const &value) : value_(value) {}
    EventCast(E &&value) : value_(std::move(value)) {}

    void inform(IEventListener<E> &listener) const { listener.onEvent(value_); }
    void inform(EventListener &listener) const override
    {
        if (auto *derivedListener = dynamic_cast<IEventListener<E>*>(&listener)) { inform(*derivedListener); }
    }

  private:
    E value_;
};

template <typename E> using event_cast = EventCast<E>;

}} // namespace ax::task
