//
// Created by Martin Miralles-Cordal on 8/24/19.
//

#pragma once

#include <ax/atomic/Queue.hpp>
#include <ax/atomic/Vector.hpp>
#include <ax/task/Event.hpp>

namespace ax {
inline namespace task {

class EventQueue
{
  public:
    template <typename T>
    void pushEvent(T &&event)
    {
        if constexpr (std::is_base_of_v<Event, T>) {
            events_.emplace(new T{std::forward<T>(event)});
        } else {
            events_.emplace(new EventCast<T>{std::forward<T>(event)});
        }
    }

    bool processNextEvent()
    {
        return (events_.tryPop() & [this](auto &&e) { notify(*e); return core::Maybe{true}; }).isSet();
    }

    void addListener(EventListener &listener) { listeners_.push_back(&listener); }
    void removeListener(EventListener &listener) { listeners_.remove(&listener); }

    void addListener(std::shared_ptr<EventListener> &listener) { weakListeners_.push_back(listener); }
    void removeListener(std::shared_ptr<EventListener> &listener)
    {
        weakListeners_.remove_if([&](auto &weak) { return listener == weak.lock(); });
    }

  private:
    void notify(Event &event)
    {
        for (auto &listener : listeners_) {
            event.inform(*listener);
        }

        weakListeners_.for_each([&event](decltype(weakListeners_)::value_type &weak) {
            if (auto shared = weak.lock()) {
                event.inform(*shared);
            }
        });

        weakListeners_.remove_if([](decltype(weakListeners_)::value_type &weak) { return weak.expired(); });
    };
    atomic::Queue<std::unique_ptr<Event>> events_;
    atomic::Vector<EventListener*> listeners_;
    atomic::Vector<std::weak_ptr<EventListener>> weakListeners_;
};

}} // namespace ax::task
