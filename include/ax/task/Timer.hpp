//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <thread>

namespace ax {
inline namespace task {

class TimerAgent;

/** The Timer class runs a function after a specified timeframe. */
class Timer
{
  public:
    using VoidFunction = std::function<void()>;
    using Function = std::function<void(bool)>;
    using Clock = std::chrono::steady_clock;
    using TimeResolution = std::chrono::milliseconds;

    class ScheduledItem;

    Timer() = default;

    /**
     * @param func The function to run. Takes no parameters.
     * @param duration How long the timer should wait from now (or the last execution) before executing the function.
     * @param repeating Whether or not the timer repeats.
     */
    template<class _Rep, class _Period>
    Timer(VoidFunction const &func, std::chrono::duration<_Rep, _Period> duration, bool repeating = false)
            : Timer{[func](bool) { func(); }, duration, repeating}
    {}

    /**
     * @param func The function to run. Takes the cancellation status as a parameter.
     * @param duration How long the timer should wait from now (or the last execution) before executing the function.
     * @param repeating Whether or not the timer repeats.
     */
    template<class _Rep, class _Period>
    Timer(Function func, std::chrono::duration<_Rep, _Period> duration, bool repeating = false)
            : impl_{InitImpl(std::move(func), Clock::now()+duration,
                             std::chrono::duration_cast<TimeResolution>(duration), repeating)}
    {}

    /**
     * @param func The function to run. Takes no parameters.
     * @param when When the timer should trigger.
     * @param repeating If true, the timer will repeat at the interval given by (when - now).
     */
    Timer(VoidFunction const &func, Clock::time_point when, bool repeating = false)
            : Timer{[func](bool) { func(); }, when, repeating}
    {}

    /**
     * @param func The function to run. Takes the cancellation status as a parameter.
     * @param when When the timer should trigger.
     * @param repeating If true, the timer will repeat at the interval given by (when - now).
     */
    Timer(Function func, Clock::time_point when, bool repeating = false)
            : impl_{InitImpl(std::move(func), when,
                             std::chrono::duration_cast<TimeResolution>(when - Clock::now()), repeating)}
    {}

    Timer(Timer &&other) = default;
    Timer(Timer const &other) = default;
    Timer &operator=(Timer const &rhs) = default;
    Timer &operator=(Timer &&rhs) = default;
    ~Timer() = default;

    [[nodiscard]] Clock::time_point scheduledTime() const;
    explicit operator bool() const;
    [[nodiscard]] bool ready() const;
    [[nodiscard]] bool readyAt(Clock::time_point time) const;
    void cancel();
    void invalidate(bool shouldExecute = false);

  private:
    void operator()();
    struct Impl;
    std::shared_ptr<Impl> InitImpl(Function func, Clock::time_point end, std::chrono::microseconds interval, bool repeating);
    std::shared_ptr<Impl> impl_;
};

class Timer::ScheduledItem
{
  public:
    ScheduledItem(uint64_t id, Timer t) : id_(id), t_(std::move(t)) {}

    [[nodiscard]] uint64_t id() const { return id_; }

    [[nodiscard]] Clock::time_point scheduledTime() const { return t_.scheduledTime(); }
    bool execute() { return t_.ready() ? t_(), true : false; }
    void cancel() { t_.cancel(); }

    explicit operator bool() const { return static_cast<bool>(t_); }
    bool operator>(const ScheduledItem &other) const { return scheduledTime() > other.scheduledTime(); }

  private:
    uint64_t id_;
    Timer t_;
};

}} //  namespace ax::task
