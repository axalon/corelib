//
// Created by Martin Miralles-Cordal on 7/30/19.
//

#pragma once

#include <ax/core/Maybe.hpp>

#include <condition_variable>
#include <mutex>

namespace ax {
inline namespace task {

class ConditionVariable
{
  public:
    using Lock = std::unique_lock<std::mutex>;
    void notify_one() noexcept { cond_.notify_one(); }
    void notify_all() noexcept { cond_.notify_all(); }

    Lock wait()
    {
        Lock lock{mutex_};
        cond_.wait(lock);
        return lock;
    }

    template<typename Predicate>
    Lock wait(Predicate p)
    {
        Lock lock{mutex_};
        cond_.wait(lock, std::move(p));
        return lock;
    }

    template<class Rep, class Period>
    [[nodiscard]] Maybe<Lock> wait_for(const std::chrono::duration<Rep, Period> &rel_time,
                                       std::cv_status *status = nullptr)
    {
        Lock lock{mutex_};
        auto timeout = cond_.wait_for(lock, rel_time);
        const bool success = timeout == std::cv_status::no_timeout;
        if (status) { *status = timeout; }
        return success ? Maybe<Lock>{std::move(lock)} : Maybe<Lock>{};
    }

    template<class Rep, class Period, class Predicate>
    [[nodiscard]] Maybe<Lock> wait_for(const std::chrono::duration<Rep, Period> &rel_time,
                                       Predicate p, std::cv_status *status = nullptr)
    {
        Lock lock{mutex_};
        bool success = cond_.wait_for(lock, rel_time, std::move(p));
        if (status) { *status = success ? std::cv_status::no_timeout : std::cv_status::timeout; }
        return success ? Maybe<Lock>{std::move(lock)} : Maybe<Lock>{};
    }

    template<class Clock, class Duration>
    [[nodiscard]] Maybe<Lock> wait_until(const std::chrono::time_point<Clock, Duration> &timeout_time,
                                         std::cv_status *status = nullptr)
    {
        Lock lock{mutex_};
        auto timeout = cond_.wait_until(lock, timeout_time);
        const bool success = timeout == std::cv_status::no_timeout;
        if (status) { *status = timeout; }
        return success ? Maybe<Lock>{std::move(lock)} : Maybe<Lock>{};
    }

    template<class Clock, class Duration, class Predicate>
    [[nodiscard]] Maybe<Lock> wait_until(const std::chrono::time_point<Clock, Duration> &timeout_time,
                                         Predicate p, std::cv_status *status = nullptr)
    {
        Lock lock{mutex_};
        bool success = cond_.wait_until(lock, timeout_time, std::move(p));
        if (status) { *status = success ? std::cv_status::no_timeout : std::cv_status::timeout; }
        return success ? Maybe<Lock>{std::move(lock)} : Maybe<Lock>{};
    }

    std::mutex &mutex() { return mutex_; }

  private:
    std::mutex mutex_;
    std::condition_variable cond_;
};

}} // namespace ax::task
