//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <condition_variable>
#include <mutex>

namespace ax {
inline namespace task {

/**
 * Thread barrier. Waits until a given number of threads or tasks reach the
 * barrier and decrement its count down to zero, at which point it opens.
 *
 * This variety of Barrier is also known as a Latch.
 *
 * Conceptually it is kind of the inverse of a semaphore. Where a semaphore will
 * wait for it to be incremented above zero, a barrier waits until it reaches
 * zero.
 */
class Barrier
{
  public:
    explicit Barrier(int count) noexcept : initialCount_{count}, count_{count} {}

    void decrement()
    {
        std::lock_guard lockGuard{mutex_};
        if (--count_ == 0) { cond_.notify_all(); }
    }

    void decrementAndWait()
    {
        std::unique_lock lock{mutex_};
        if (--count_ == 0) {
            cond_.notify_all();
        } else {
            cond_.wait(lock, [this] { return count_ <= 0; });
        }
    }

    void wait() const
    {
        std::unique_lock lock{mutex_};
        cond_.wait(lock, [this] { return count_ <= 0; });
    }

    bool tryWait() const
    {
        std::lock_guard lockGuard{mutex_};
        return count_ <= 0;
    }

    /** Resets the barrier if the barrier has been opened. */
    bool reset()
    {
        std::lock_guard lockGuard{mutex_};
        const bool willReset = count_ <= 0;
        if (willReset) { count_ = initialCount_; }
        return willReset;
    }

  private:
    mutable std::mutex mutex_;
    mutable std::condition_variable cond_;
    const int initialCount_;
    int count_;
};

}} // namespace ax::task
