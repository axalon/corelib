//
// Created by Martin Miralles-Cordal on 9/29/19.
//

#pragma once
#include <ax/task/ConditionVariable.hpp>

namespace ax {
inline namespace task {

class PunchClock
{
 public:
  void punchIn()
  {
    std::lock_guard<std::mutex> lockGuard{mutex()};
    ++activeWorkers_;
  }

  void punchOut()
  {
    std::lock_guard<std::mutex> lockGuard{mutex()};
    if (activeWorkers_ > 0) {
      if (--activeWorkers_ == 0) { cond_.notify_all(); }
    }
  }

  template<class Rep, class Period>
  bool waitUntilEmptyFor(const std::chrono::duration<Rep, Period>& rel_time) const
  {
    return cond_.wait_for(rel_time, [this] { return empty_(); }).isSet();
  }

  void waitUntilEmpty() const { cond_.wait([this] { return empty_(); }); }

  bool empty() const
  {
    std::lock_guard<std::mutex> lockGuard{mutex()};
    return empty_();
  }

 private:
  std::mutex &mutex() const { return cond_.mutex(); }
  bool empty_() const { return activeWorkers_ == 0; }
  mutable ConditionVariable cond_;
  unsigned int activeWorkers_{0u};
};

}} // namespace ax::task
