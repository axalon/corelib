//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <condition_variable>
#include <mutex>

namespace ax {
inline namespace task {

class Semaphore
{
  public:
    Semaphore(int count = 0)
            : mutex_()
            , cond_()
            , count_(count) {}

    void notify()
    {
        std::lock_guard lockGuard{mutex_};
        ++count_;
        cond_.notify_one();
    }

    void wait()
    {
        std::unique_lock lock{mutex_};
        cond_.wait(lock, [this] { return count_ > 0; });
        --count_;
    }

    bool tryWait()
    {
        std::lock_guard lockGuard{mutex_};
        if (count_ > 0) {
            --count_;
            return true;
        } else {
            return false;
        }
    }

    template<class Clock, class Duration>
    bool waitUntil(const std::chrono::time_point<Clock, Duration> &point)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if (!cond_.wait_until(lock, point, [this]() { return count_ > 0; })) {
            return false;
        }
        --count_;
        return true;
    }

  private:
    std::mutex mutex_;
    std::condition_variable cond_;
    int count_;
};

}} // namespace ax::task
