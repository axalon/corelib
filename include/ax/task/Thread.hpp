//
// Created by Martin Miralles-Cordal on 7/26/19.
//

#pragma once

#include <functional>
#include <memory>

namespace ax {
inline namespace task {

class Thread
{
  public:
    Thread(char const *name = nullptr);

    template<class Function, class... Args>
    void start(Function&& f, Args&&... args) { start(std::function{[=]() { f(args...); }}); }
    void start(std::function<void()> body);

    std::string name() const;
    void setName(std::string name);

  private:
    class Impl;
    Thread(std::unique_ptr<Impl> &&impl);
    std::unique_ptr<Impl> impl_;
};

}} // namespace ax::task
