//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <ax/task/Semaphore.hpp>
#include <ax/task/Timer.hpp>
#include <ax/Defines.hpp>

#include <cassert>
#include <chrono>
#include <functional>
#include <queue>

namespace ax {
inline namespace task {

class TimerQueue
{
  public:
    TimerQueue() = default;

    ~TimerQueue()
    {
        cancelAll();
        // Abusing the timer queue to trigger the shutdown.
        add(0, [this](bool) { finish_ = true; });
    }


    //! Adds a new timer
    // \return
    //  Returns the ID of the new timer. You can use this ID to cancel the
    // timer
    uint64_t add(int64_t milliseconds, std::function<void()> handler)
    {
        return add(Timer{handler, Clock::now() + std::chrono::milliseconds(milliseconds)});
    }

    //! Adds a new timer
    // \return
    //  Returns the ID of the new timer. You can use this ID to cancel the
    // timer
    uint64_t add(int64_t milliseconds, std::function<void(bool)> handler)
    {
        return add(Timer{std::move(handler), Clock::now() + std::chrono::milliseconds(milliseconds)});
    }

    uint64_t add(Timer const &timer)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        uint64_t const id = ++idcounter_;
        Timer::ScheduledItem item{id, timer};
        items_.push(std::move(item));
        lock.unlock();
        // Something changed, so wake up timer thread
        checkWork_.notify();
        return id;
    }

    bool cancel(uint64_t id)
    {
        // Instead of removing the item from the container (thus breaking the
        // heap integrity), we move the Timer out of the item and put it into a
        // new item at the top for immediate execution. The timer thread will
        // then ignore the original item, since it has no Timer.
        std::unique_lock<std::mutex> lock(mutex_);
        for (auto &&item : items_.getContainer()) {
            if (item && item.id() == id) {
                Timer::ScheduledItem newItem{std::move(item)};
                assert(bool(item) == false);
                newItem.cancel();
                items_.push(std::move(newItem));
                lock.unlock();
                // Something changed, so wake up timer thread
                checkWork_.notify();
                return true;
            }
        }

        return false;
    }

    size_t cancelAll()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        for (auto &&item : items_.getContainer()) {
            if (item) {
                item.cancel();
            }
        }
        auto ret = items_.size();

        lock.unlock();
        checkWork_.notify();
        return ret;
    }

    bool tryWaitForNextTimer()
    {
        const auto [hasWork, nextTimer] = calcWaitTime();
        if (hasWork) {
            checkWork_.waitUntil(nextTimer);
            executeReadyTimers();
            return true;
        } else {
            return false;
        }
    }

    void waitForNextTimer()
    {
        if (!tryWaitForNextTimer())
        {
            checkWork_.wait();
            executeReadyTimers();
        }
    }

    void executeReadyTimers()
    {
        std::vector<Timer::ScheduledItem> readyTimers;
        // collect the timers
        synchronized(mutex_) {
            while (!items_.empty() && items_.top().scheduledTime() <= Clock::now()) {
                readyTimers.emplace_back(items_.top());
                items_.pop();
            }
        }

        // execute the timers
        if (!readyTimers.empty()) {
            std::vector<Timer::ScheduledItem> repeatingTimers;
            for (auto &&item : readyTimers) {
                item.execute();
                // re-queue repeating timers
                if (item) {
                    repeatingTimers.emplace_back(std::move(item));
                }
            }

            if (!repeatingTimers.empty()) {
                synchronized(mutex_) {
                    for (auto &&item : repeatingTimers) {
                        items_.push(std::move(item));
                    }
                }
            }
        }
    }

    [[nodiscard]] bool empty() const { return items_.empty(); }

  private:
    using Clock = std::chrono::steady_clock;

    void run()
    {
        while (!finish_) { waitForNextTimer(); }
        assert(items_.size() == 0);
    }

    std::pair<bool, Clock::time_point> calcWaitTime()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        while (items_.size()) {
            if (items_.top()) {
                // Item present, so return the new wait time
                return std::make_pair(true, items_.top().scheduledTime());
            } else {
                // Discard empty handlers (they were cancelled)
                items_.pop();
            }
        }

        // No items found, so return no wait time (causes the thread to wait indefinitely)
        return std::make_pair(false, Clock::time_point());
    }

    Semaphore checkWork_;
    bool finish_ = false;
    uint64_t idcounter_ = 0;
    std::mutex mutex_;

    class Queue : public std::priority_queue<Timer::ScheduledItem, std::vector<Timer::ScheduledItem>, std::greater<>>
    {
      public:
        std::vector<Timer::ScheduledItem> &getContainer()
        {
            return this->c;
        }
    } items_;
};

}} // namespace ax::task
