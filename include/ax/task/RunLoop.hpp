//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <ax/task/EventQueue.hpp>
#include <ax/task/TimerQueue.hpp>

#include <ax/atomic/Vector.hpp>
#include <ax/atomic/Queue.hpp>
#include <ax/core/Signal.hpp>

#include <atomic>

namespace ax {
inline namespace task {

class RunLoop
{
  public:
    void run()
    {
        while (!done_)
        {
            timers_.tryWaitForNextTimer();
        }
    }

  private:
    std::atomic_bool done_{false};
    TimerQueue timers_;
};

}} // namespace ax::task