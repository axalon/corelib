//
// Created by Martin Miralles-Cordal on 9/29/19.
//

#pragma once

#include <function2/function2.hpp>
#include <ax/core/TypeUtils.hpp>

namespace ax {
inline namespace task {

AX_TAG_TYPE(DeferredStartTag, kDeferStart);

/** Owning polymorphic container for any Executor. */
class Executor final
{
    template<typename T>
    using EnableIfDifferentType = std::enable_if_t<!std::is_same_v<std::decay_t<T>, Executor>>;
  public:
    template<typename T, typename...Args>
    static Executor Create(Args &&...args) { return Executor{T{args...}}; }

    template<typename Impl, typename = EnableIfDifferentType<Impl>>
    Executor(Impl impl) : impl_{std::make_unique<Model<std::decay_t<Impl>>>(std::move(impl))} {}

    template<typename Impl, typename = EnableIfDifferentType<Impl>>
    Executor &operator=(Impl impl)
    {
        impl_ = std::make_unique<Model<std::decay_t<Impl>>>(std::move(impl));
        return *this;
    }

    Executor(Executor &&rhs) noexcept = default;
    Executor &operator=(Executor &&rhs) noexcept = default;

    void dispatch(fu2::function<void() const> f) { impl_->dispatch(std::move(f)); }
    void schedule(fu2::function<void() const> f) { impl_->schedule(std::move(f)); }

  private:
    struct Concept
    {
        virtual ~Concept() = default;
        virtual void dispatch(fu2::function<void() const> f) = 0;
        virtual void schedule(fu2::function<void() const> f) = 0;
    };

    template<typename ExecutorImpl>
    struct Model final : Concept
    {
        explicit Model(ExecutorImpl data) : data_{std::move(data)} {}
        void dispatch(fu2::function<void() const> f) override { data_.dispatch(std::move(f)); }
        void schedule(fu2::function<void() const> f) override { data_.schedule(std::move(f)); }

        ExecutorImpl data_;
    };

    std::unique_ptr<Concept> impl_;
};

/** Non-owning reference container for any Executor. */
class ExecutorView final
{
  public:
    template<typename Impl>
    explicit ExecutorView(Impl &impl) : impl_{} { new(std::addressof(impl_)) Model <Impl>{std::ref(impl)}; }

    ExecutorView(const ExecutorView &rhs) : impl_{} { rhs.data().copy(std::addressof(impl_)); }
    ExecutorView &operator=(const ExecutorView &rhs)
    {
        data().~Concept();
        rhs.data().copy(std::addressof(impl_));
        return *this;
    }

    ~ExecutorView() = default;

    void dispatch(fu2::function<void() const> f) const { data().dispatch(std::move(f)); }
    void schedule(fu2::function<void() const> f) const { data().schedule(std::move(f)); }

  private:
    struct Concept
    {
        virtual ~Concept() = default;
        virtual void dispatch(fu2::function<void() const> f) const = 0;
        virtual void schedule(fu2::function<void() const> f) const = 0;
        virtual void copy(void *dest) const = 0;
    };

    template<typename ExecutorImpl>
    struct Model final : Concept
    {
        explicit Model(std::reference_wrapper<ExecutorImpl> data) : data_{std::move(data)} {}
        void dispatch(fu2::function<void() const> f) const override { data_.get().dispatch(std::move(f)); }
        void schedule(fu2::function<void() const> f) const override { data_.get().schedule(std::move(f)); }
        void copy(void *dest) const override { new(dest) Model{*this}; }

        std::reference_wrapper<ExecutorImpl> data_;
    };

    [[nodiscard]] const Concept& data() const { return *reinterpret_cast<const Concept *>(std::addressof(impl_)); }
    [[nodiscard]] Concept & data() { return *reinterpret_cast<Concept *>(std::addressof(impl_)); }

    std::aligned_union_t<8, Model<int>> impl_;
};

}} // namespace ax::task
