//
// Created by Martin Miralles-Cordal on 7/26/19.
//

#pragma once

#include <ax/atomic/Queue.hpp>
#include <ax/task/ConditionVariable.hpp>
#include <ax/task/PunchClock.hpp>
#include <ax/task/Task.hpp>

#include <array>
#include <future>
#include <mutex>
#include <thread>
#include <utility>

namespace ax {
inline namespace task {

class Queue
{
  public:
    class Worker;

    enum class State
    {
        Invalid,
        Running,
        Paused,
    };

    Queue() = default;
    Queue(Queue const &rhs) : queue_{rhs.queue_}, cond_{}, state_{rhs.state_.load()} {}
    Queue& operator=(Queue const &rhs)
    {
        queue_ = rhs.queue_;
        state_ = rhs.state_.load();
        return *this;
    }

    Queue(Queue &&rhs) noexcept : queue_{std::move(rhs.queue_)}, cond_{}, state_{rhs.state_.load()} {}
    Queue& operator=(Queue &&rhs) noexcept
    {
        queue_ = std::move(rhs.queue_);
        state_ = rhs.state_.load();
        return *this;
    }

    [[nodiscard]] bool invalidate()
    {
        const bool stateDidChange = state_.exchange(State::Invalid) != State::Invalid;
        if (stateDidChange) { cond_.notify_all(); }
        return stateDidChange;
    }

    bool pause() { return setState(Expected{State::Running}, Target{State::Paused}); }
    bool resume()
    {
        const bool didresume = setState(Expected{State::Paused}, Target{State::Running});
        if (didresume) { cond_.notify_all(); }
        return didresume;
    }


    [[nodiscard]] State state() const { return state_; }
    void overrideState(State state) { state_ = state; }

    [[nodiscard]] bool valid() const { return state_ != State::Invalid; }

    [[nodiscard]] bool invalid() const { return state_ == State::Invalid; }
    [[nodiscard]] bool paused() const { return state_ == State::Paused; }
    [[nodiscard]] bool running() const { return state_ == State::Running; }
    
    void push(Task const &t) { push(t.function()); }
    void push(std::function<void()> func)
    {
        queue_.emplace(std::move(func));
        notify();
    }

    template<typename F, typename...Args>
    auto dispatch(F&& f, Args&&... args) -> std::future<std::invoke_result_t<F, Args...>>
    {
        using QueueTask = std::packaged_task<std::invoke_result_t<F, Args...>()>;
        auto task = std::make_shared<QueueTask>(Invocation(std::forward<F>(f), std::forward<Args>(args)...));
        push([task](){ (*task)(); });
        return task->get_future();
    }

    [[nodiscard]] bool hasJobs() const { return !queue_.empty(); }

    void notify() { cond_.notify_one(); }
    void notifyAll() { cond_.notify_all(); }

  protected:
    struct Expected { State value; };
    struct Target { State value; };
    bool setState(Expected e, Target t)
    {
        State testval = e.value;
        return state_.compare_exchange_strong(testval, t.value);
    }

  private:
    atomic::Queue<std::function<void()>> queue_;
    std::condition_variable cond_;
    std::atomic<State> state_{State::Paused};
};

class Queue::Worker
{
  public:
    Worker(uint32_t id, Queue &pool, PunchClock &punchClock) : id_(id), pool_(pool), punchClock_{punchClock} {}

    Worker(Worker const &rhs) : id_{rhs.id_}, pool_{rhs.pool_}, punchClock_{rhs.punchClock_}, mutex_{} {}
    Worker& operator=(Worker const &rhs) { setMembers(rhs); return *this; }

    Worker(Worker &&rhs) noexcept : id_{rhs.id_}, pool_{rhs.pool_}, punchClock_{rhs.punchClock_}, mutex_{} {}
    Worker& operator=(Worker &&rhs) { setMembers(std::move(rhs)); return *this; }

    [[nodiscard]] uint32_t id() const { return id_; }
    void operator()();

    [[nodiscard]] bool paused() const { return pool_.get().paused(); }
    [[nodiscard]] bool Running() const { return pool_.get().running(); }

    void reassign(Queue &newPool);

  private:
    [[nodiscard]] bool canWork() const;
    void tryToWork();

    template <typename TMember> static void setMember(Worker &lhs, Worker const &rhs, TMember Worker::*memptr) { lhs.*memptr = rhs.*memptr; }
    template <typename TMember> static void setMember(Worker &lhs, Worker &&rhs, TMember Worker::*memptr) { lhs.*memptr = std::move(rhs.*memptr); }

    template <typename Rhs>
    void setMembers(Rhs &&rhs)
    {
        setMember(*this, rhs, &Worker::id_);
        setMember(*this, rhs, &Worker::pool_);
        setMember(*this, rhs, &Worker::punchClock_);
    }

    uint32_t id_;
    std::reference_wrapper<Queue> pool_;
    std::reference_wrapper<PunchClock> punchClock_;
    std::mutex mutex_;
};

}} // namespace ax::task
