//
// Created by Martin Miralles-Cordal on 7/26/19.
//

#pragma once

#include <ax/core/TypeUtils.hpp>

#include <cassert>
#include <future>

namespace ax {
inline namespace task {

class Task
{
  public:
    // not a constructor because of ambiguity over the resolution of the F type and the copy constructor.
    template<typename F, typename...Args> static Task make(F &&f, Args&&... args);

    // Not a method to avoid the dependent type nonsense which forces the call to look like `t.template future<T>()`.
    // This is also why std::get<int>(std::tuple) is not a member function of std::tuple.
    template <typename T>
    [[nodiscard]] static std::future<T> future(Task &t);

    Task(const Task &other) = default;
    Task& operator=(const Task &other) = default;
    Task(Task &&other) noexcept = default;
    Task& operator=(Task &&other) noexcept = default;

    [[nodiscard]] std::function<void()> function() const { return impl_->function(); }
    void run() { impl_->operator()(); }
    void operator()() { run(); }

  private:
    class ITaskBase
    {
      public:
        virtual ~ITaskBase() {}
        [[nodiscard]] virtual std::function<void()> function() const = 0;
        virtual void operator()() = 0;
    };

    Task(std::shared_ptr<ITaskBase> impl) : impl_(std::move(impl)) {}

    template <typename T>
    class ITask : public ITaskBase
    {
      public:
        [[nodiscard]] virtual std::future<T> future() = 0;
    };

    template <typename T> using _future = std::invoke_result_t<decltype(&T::future), T>;
    template <typename T> using future_type_t = std::invoke_result_t<decltype(&_future<T>::get), _future<T>>;

    template <typename T>
    class Model : public ITask<future_type_t<T>>
    {
      public:
        Model(T const &v) : underlying_(v) {}
        Model(T &&v) : underlying_(std::move(v)) {}
        [[nodiscard]] std::function<void()> function() const override { return underlying_.function(); }
        [[nodiscard]] std::future<future_type_t<T>> future() override { return underlying_.future(); }
        void operator()() override { underlying_(); }
      private:
        T underlying_;
    };

    std::shared_ptr<ITaskBase> impl_;
};

namespace detail {

template<typename T>
struct PackagedTask
{
  public:
    template <typename Func>
    PackagedTask(Func &&func) : task_{std::make_shared<std::packaged_task<T()>>(std::forward<Func>(func))} {}

    [[nodiscard]] std::future<T> future() { return task_->get_future(); }
    [[nodiscard]] std::function<void()> function() const { return [task = task_] { (*task)(); }; }
    void operator()() { (*task_)(); }

    std::shared_ptr<std::packaged_task<T()>> task_;
};

}

template<typename F, typename...Args>
Task Task::make(F &&f, Args&&... args)
{
    auto func = Invocation(std::forward<F>(f), std::forward<Args>(args)...);
    auto impl = detail::PackagedTask<decltype(f(args...))>{std::move(func)};
    return Task{std::make_shared<Model<std::decay_t<decltype(impl)>>>(std::move(impl))};
}

template <typename T>
[[nodiscard]] std::future<T> Task::future(Task &t)
{
    auto *model = dynamic_cast<ITask<T>*>(t.impl_.get());
    assert(model);
    return model->future();
}

}} // namespace ax::task
