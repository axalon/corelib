//
// Created by Martin Miralles-Cordal on 8/21/19.
//

#pragma once

#include <atomic>

namespace ax {

class SpinMutex
{
  public:
    void lock() { while (!try_lock()) { /* spin spin spin, spin the lock */ }}
    void unlock() { resource_.clear(); }
    [[nodiscard]] bool try_lock() { return !resource_.test_and_set(); }

  private:
    std::atomic_flag resource_;
};

using spin_mutex = SpinMutex; // for symmetry with std types

} // namespace ax
