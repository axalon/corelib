//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <functional>
#include <mutex>
#include <shared_mutex>

namespace ax::atomic {

template <typename Mutex = std::mutex>
class BasicInvoker
{
  public:
    template<class F, class... Args>
    decltype(auto) invoke(F&& f, Args&&... args) const
    {
        std::lock_guard lockGuard{mutex_};
        return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    }

    template<class F, class... Args>
    decltype(auto) invoke_shared(F&& f, Args&&... args) const
    {
        std::shared_lock lock{mutex_};
        return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    }

  private:
    mutable Mutex mutex_;
};

using Invoker = BasicInvoker<>;
using SharedInvoker = BasicInvoker<std::shared_mutex>;

} // namespace ax::atomic
