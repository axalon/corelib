//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include <ax/core/Maybe.hpp>

#include <condition_variable>
#include <deque>
#include <mutex>

namespace ax::atomic {

/** Thread-safe multiple producer, multiple consumer queue. */
template<typename T>
class Queue
{
  public:
    using value_type = T;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer_type = value_type *;
    using size_type = std::size_t;
    using difference_type = size_type;

    Queue() = default;

    Queue(Queue const &rhs) : queue_{rhs.queue_}, mutex_{}, cond_{} {}
    Queue& operator=(Queue const &rhs)
    {
        queue_ = rhs.queue_;
        return *this;
    }
    Queue(Queue &&rhs) : queue_{std::move(rhs.queue_)}, mutex_{}, cond_{} {}
    Queue& operator=(Queue &&rhs)
    {
        queue_ = std::move(rhs.queue_);
        return *this;
    }

    template <typename... Args>
    void emplace(Args&&... args)
    {
        synchronized(mutex_) { queue_.emplace_back(std::forward<Args>(args)...); }
        cond_.notify_one();
    }
    
    void push(const T &item)
    {
        synchronized(mutex_) { queue_.push_back(item); }
        cond_.notify_one();
    }

    void push(T &&item)
    {
        synchronized(mutex_) { queue_.push_back(std::move(item)); }
        cond_.notify_one();
    }

    T pop()
    {
        std::unique_lock mlock(mutex_);
        cond_.wait(mlock, notEmpty(this));
        auto item = std::move(queue_.front());
        queue_.pop_front();
        return item;
    }

    T peek() const
    {
        std::unique_lock mlock(mutex_);
        cond_.wait(mlock, notEmpty(this));
        return queue_.front();
    }

    core::Maybe<T> tryPop()
    {
        std::unique_lock mlock(mutex_);
        if (!queue_.empty()) {
            core::Maybe<T> item{std::move(queue_.front())};
            queue_.pop_front();
            return item;
        } else {
            return core::Nothing{};
        }
    }

    template <typename Rep, typename Period>
    core::Maybe<T> tryPop(std::chrono::duration<Rep, Period> timeout)
    {
        std::unique_lock mlock(mutex_);
        if (cond_.wait_for(mlock, timeout, notEmpty(this))) {
            auto item = std::move(queue_.front());
            queue_.pop_front();
            return item;
        } else {
            return core::Nothing{};
        }
    }

    size_t size() const
    {
        std::lock_guard mlock(mutex_);
        return queue_.size();
    }

    bool empty() const
    {
        std::lock_guard mlock(mutex_);
        return queue_.empty();
    }

    void clear()
    {
        std::lock_guard mlock(mutex_);
        queue_.clear();
    }

  private:
    static constexpr auto notEmpty(Queue *q) { return [q]{ return !q->queue_.empty(); }; };
    std::deque<T> queue_;
    mutable std::mutex mutex_;
    std::condition_variable cond_;
};

} // namespace ax::atomic
