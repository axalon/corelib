//
// Created by Martin Miralles-Cordal on 8/21/19.
//

#pragma once

#include <array>
#include <atomic>
#include <chrono>
#include <map>
#include <memory>
#include <numeric>
#include <thread>

#include <ax/hash.hpp>
#include <ax/SpinMutex.hpp>

#ifndef AX_CURATOR_SLEEP
    #define AX_CURATOR_SLEEP std::chrono::milliseconds(30000)
#endif

namespace ax::atomic {

struct CacheObjectLockedException : std::exception
{
    [[nodiscard]] char const *what() const noexcept override { return "Object is currently locked"; };
};

enum class CacheWriteMode
{
    Always,
    IfSet,
    IfNotSet
};

/**
 * Concurrent cache type.
 * @tparam Key The type of key for the cached value.
 * @tparam T The type of cached value.
 * @tparam ShardCt Total number of cache shards. This should be much larger 
 *                 than the number of threads likely to access the cache at any
 *                 one time.
 */
template<class Key, class T, size_t ShardCt>
class Cache
{
  public:
    using Clock = std::chrono::system_clock;
    using Timestamp = Clock::time_point;

  private:
    class Shard;
    class CacheItem;

  public:
    Cache() : shards_{}, done_{false}, curator_{&Cache::curate, this} {}

    ~Cache()
    {
        done_ = true;
        curator_.join();
    };

    /**
     * Get some metrics
     *
     * @retval
     */
    size_t size()
    {
        size_t total_size = std::accumulate(shards_.begin(), shards_.end(), size_t{0}, [](size_t acc, auto &shard) {
            auto lock = shard.guard();
            return acc + shard->size();
        });

        return total_size;
    };

    /**
     * Set a value into the cache
     *
     * @param id the key
     * @param val shared_ptr to the object to set
     * @param expiration time
     * @param mode the write mode
     * @return True if set, false otherwise.
     */
    bool add(Key id, std::shared_ptr<T> val, time_t expiration = 0,
               const CacheWriteMode mode = CacheWriteMode::Always)
    {
        using namespace std::literals;

        // Get shard
        auto index = calcIndex(id);
        auto &shard = shards_.at(index);

        // Lock and write
        auto lock = shard.guard();
        if (mode == CacheWriteMode::IfSet)
        {
            if (!shard.contains(id)) {
                return 0;
            } else {
                shard.erase(id);
            }
        }

        auto result = shard.emplace(id, CacheItem{val, expiration});
        if (!result.second) {
            // Key exists, so nothing was written
            if (mode == CacheWriteMode::IfNotSet) {
                return 0;
            } else {
                shard.erase(id);
                shard.emplace(id, CacheItem{val, expiration});
                return 1;
            }
        }

        return 1;
    };

    /**
     * Find if a key exists
     *
     * @param id the key
     * @retval 1 if the key exists, 0 otherwise
     */
    size_t exists(Key id)
    {
        return (get(id)) ? 1 : 0;
    };

    /**
     * Delete a value from the cache
     *
     * @param id the key
     * @retval the number of items erased
     */
    size_t erase(Key id)
    {
        // Get shard
        size_t index = calcIndex(id);
        auto &shard = shards_.at(index);

        // Lock and erase
        auto lock = shard.guard();
        return shard.erase(id);
    };

    /** Get a value from the cache */
    std::shared_ptr<T> get(Key id)
    {
        // Get shard
        size_t index = this->calcIndex(id);
        auto &shard = this->shards_.at(index);

        // Lock
        auto lock = shard.guard();

        // OK, we now have exclusive access to the shard.  So no race condition is possible for the affections of this item...
        CacheItem item;

        if (shard.contains(id)) {
            item = shard.at(id);
            if (item->expired()) {
                shard.erase(id);
                return std::shared_ptr<T>();
            }
        }

        // If we are allowing mutables, make sure no one else is using this data!
        #ifdef FASTCACHE_MUTABLE_DATA
        if(!item->data.unique()) {
            throw CacheObjectLockedException();
        }
        #endif

        return item->data;
    };

  protected:
    void curate()
    {
        while (!done_) {
            // Snooze a little
            std::this_thread::sleep_for(AX_CURATOR_SLEEP);

            // Iterate all objects in cache, checking for expired objects
            for (auto &shard : shards_)
            {
                std::lock_guard lock(*shard->guard);
                shard.cullExpiredKeys();
            }
        }
    };

    size_t calcIndex(Key id)
    {
        return (size_t) this->hash_(id) % ShardCt;
    };

  private:
    class CacheItem
    {
      public:
        CacheItem() = default;
        CacheItem(std::shared_ptr<T> data, Timestamp expiration)
                : data_(std::move(data)), expiration_(expiration) {}

        /** Whether or not the item has expired and can be removed. */
        [[nodiscard]] bool expired() const
        {
            if (expiration_ == 0) { return false; }
            return Clock::now() > expiration_;
        };

        [[nodiscard]] std::shared_ptr<T> data() const { return data_; }

      private:
        std::shared_ptr<T> data_;
        Timestamp expiration_;
    };

    class Shard
    {
      public:
        using Value = CacheItem;
        Shard() : guard_() {}

        void cullExpiredKeys()
        {
            map_.erase(std::remove_if(map_.begin(), map_.end(), [](auto &pair) { return pair.second->expired(); }),
                       map_.end());
        }

        [[nodiscard]] size_t size() const { return map_.size(); }
        [[nodiscard]] bool contains(Key id) const { return map_.count(id) > 0; }
        [[nodiscard]] Value at(Key id) const { return map_.at(id); }
        [[nodiscard]] auto erase(Key id) const { return map_.erase(id); }

        template <typename...Ts>
        auto emplace(Ts&&...ts) { return map_.emplace(std::forward<Ts>(ts)...); }

        std::lock_guard<ax::SpinMutex> guard() { return guard_; }

      private:
        ax::SpinMutex guard_;
        std::map<Key, Value> map_;
    };

    ax::hash<Key> hash_;
    std::array<Shard, ShardCt> shards_;
    std::atomic_bool done_;
    std::thread curator_;
};

} // namespace ax::atomic
