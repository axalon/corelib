//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#include "Invoker.hpp"

#include <functional>
#include <mutex>
#include <vector>

namespace ax::atomic {

template<typename BaseIt>
class Iterator
{
  protected:
    BaseIt it_;
    SharedInvoker const &invoker_;

  public:
    using iterator_type     = typename BaseIt::iterator_type;
    using iterator_category = typename BaseIt::iterator_category;
    using value_type        = typename BaseIt::value_type;
    using difference_type   = typename BaseIt::difference_type;
    using reference         = typename BaseIt::reference;
    using pointer           = typename BaseIt::pointer;

    Iterator(BaseIt const& i, SharedInvoker const &invoker) : it_(i), invoker_(invoker) {}
    Iterator(Iterator const &other) : it_(other.it_), invoker_(other.invoker_) {}
    Iterator &operator=(Iterator const &other) { it_ = other.it_; return *this; }

    // Forward iterator requirements
    reference operator*() const { auto it = invoker_.invoke_shared([this] { return it_; }); return *it; }

    pointer operator->() const { return invoker_.invoke_shared([this] { return it_; }); }

    Iterator& operator++() { invoker_.invoke([this] { ++it_; }); return *this; }

    Iterator operator++(int) { return invoker_.invoke([this] { return AtomicIterator(it_++); }); }

    // Bidirectional iterator requirements
    Iterator& operator--() { invoker_.invoke([this] { --it_; }); return *this; }

    Iterator operator--(int) { return invoker_.invoke([this] { return AtomicIterator(it_--); }); }

    // Random access iterator requirements
    reference operator[](difference_type n) const { return invoker_.invoke_shared([this, n] { return it_[n]; }); }
    Iterator& operator+=(difference_type n) { invoker_.invoke([this, n] { it_ += n; }); return *this; }
    Iterator operator+(difference_type n) const { return invoker_.invoke_shared([this, n] { return AtomicIterator(it_ + n); }); }
    Iterator& operator-=(difference_type n) { invoker_.invoke([this, n] { it_ -= n; }); return *this; }
    Iterator operator-(difference_type n) const { return invoker_.invoke_shared([this, n] { return AtomicIterator(it_ - n); }); }
    difference_type operator-(Iterator const &rhs) const { return invoker_.invoke_shared([&] { return it_ - rhs.it_; }); }

    const BaseIt& base() const { return invoker_.invoke_shared([this] { return it_; }); }

    constexpr bool operator==(Iterator const &rhs) const { return it_ == rhs.it_; }
    constexpr bool operator!=(Iterator const &rhs) const { return it_ != rhs.it_; }
    constexpr bool operator<(Iterator const &rhs) const { return it_ < rhs.it_; }
    constexpr bool operator<=(Iterator const &rhs) const { return it_ <= rhs.it_; }
    constexpr bool operator>=(Iterator const &rhs) const { return it_ >= rhs.it_; }
    constexpr bool operator>(Iterator const &rhs) const { return it_ > rhs.it_; }
};

template <typename T>
class Vector
{
    using UT = std::vector<T>;
  public:
    using ut_const_iterator = typename std::vector<T>::const_iterator;
    using ut_iterator = typename std::vector<T>::iterator;

    using value_type = typename std::vector<T>::value_type;
    using size_type = typename std::vector<T>::size_type;
    using reference = typename std::vector<T>::reference;
    using const_reference = typename std::vector<T>::const_reference;
    using iterator = Iterator<ut_iterator>;
    using const_iterator = Iterator<ut_const_iterator>;

    Vector() = default;
    ~Vector() = default;

    Vector(std::initializer_list<T> init) : vector_{init} {}
    Vector& operator=(std::initializer_list<T> init) { return operator=(UT{init}); }

    explicit Vector(size_type count) : vector_{count} {}
    Vector(size_type count, T const &value) : vector_{count, value} {}

    Vector(Vector const &other) : vector_{other.ut_copy()} {}
    Vector& operator=(Vector const &rhs) { return operator=(rhs.vector_); }
    Vector(Vector &&other) : vector_{other.ut_move()} {}
    Vector& operator=(Vector &&rhs) { return operator=(std::move(rhs.vector_)); }

    explicit Vector(UT const &other) : vector_(other) {}
    Vector& operator=(UT const &rhs)
    {
        invoker_.invoke(static_cast<UT&(UT::*)(UT const&)>(&UT::operator=), vector_, rhs);
        return *this;
    }

    explicit Vector(UT &&other) : vector_(std::move(other)) {}
    Vector& operator=(UT &&rhs)
    {
        invoker_.invoke(static_cast<UT&(UT::*)(UT &&)>(&UT::operator=), vector_, std::move(rhs));
        return *this;
    }

    template<class InputIt> void assign(InputIt first, InputIt last) { invoker_.invoke(static_cast<void(UT::*)(InputIt,InputIt)>(&UT::assign), vector_, first, last); }
    void assign(size_type count, T const &value) { invoker_.invoke(static_cast<void(UT::*)(size_type,T const&)>(&UT::assign), vector_, count, value); }
    void assign(std::initializer_list<T> list) { invoker_.invoke(static_cast<void(UT::*)(std::initializer_list<T>)>(&UT::assign), vector_, list); }

    reference at(size_type pos) { return invoker_.invoke_shared(static_cast<reference(UT::*)(size_type)>(&UT::at), vector_, pos); }
    const_reference at(size_type pos) const { return invoker_.invoke_shared(static_cast<const_reference(UT::*)(size_type) const>(&UT::at), vector_, pos); }

    reference operator[](size_type pos) { return invoker_.invoke(static_cast<reference(UT::*)(size_type)>(&UT::operator[]), vector_, pos); }
    const_reference operator[](size_type pos) const { return invoker_.invoke(static_cast<const_reference(UT::*)(size_type) const>(&UT::operator[]), vector_, pos); }

    reference front() { return invoker_.invoke_shared(static_cast<reference(UT::*)()>(&UT::front), vector_); }
    const_reference front() const { return invoker_.invoke_shared(static_cast<const_reference(UT::*)() const>(&UT::front), vector_); }

    reference back() { return invoker_.invoke_shared(static_cast<reference(UT::*)()>(&UT::back), vector_); }
    const_reference back() const { return invoker_.invoke_shared(static_cast<const_reference(UT::*)() const>(&UT::back), vector_); }

    bool empty() const { return invoker_.invoke_shared(&UT::empty, vector_); }

    size_type size() const { return invoker_.invoke_shared(&UT::size, vector_); }

    size_type max_size() const { return invoker_.invoke_shared(&UT::max_size, vector_); }

    void reserve(size_type newCap) { invoker_.invoke(&UT::reserve, vector_, newCap); }

    size_type capacity() const { return invoker_.invoke_shared(&UT::capacity, vector_); }

    void clear() { invoker_.invoke(&UT::clear, vector_); }

    void push_back(T const &value) { invoker_.invoke(static_cast<void(UT::*)(T const&)>(&UT::push_back), vector_, value); }
    void push_back(T &&value) { invoker_.invoke(static_cast<void(UT::*)(T&&)>(&UT::push_back), vector_, std::forward<T>(value)); }

    template<class... Args>
    void emplace_back(Args&&... args) { invoker_.invoke(&UT::emplace_back, vector_, std::forward<Args>(args)...); }

    void erase(ut_const_iterator pos) { invoker_.invoke(static_cast<ut_iterator(UT::*)(ut_const_iterator)>(&UT::erase), vector_, pos); }
    void erase(ut_const_iterator first, ut_const_iterator last)
    {
        invoker_.invoke(static_cast<ut_iterator(UT::*)(ut_const_iterator, ut_const_iterator)>(&UT::erase), vector_, first, last);
    }

    void remove(T const &value) { invoker_.invoke([this, &value] { vector_.erase(std::remove(vector_.begin(), vector_.end(), value)); }); }

    template <typename UnaryPredicate>
    void remove_if(UnaryPredicate f) { invoker_.invoke([&] { vector_.erase(std::remove_if(vector_.begin(), vector_.end(), f)); }); }

    template <typename Func>
    void for_each(Func f) const { invoker_.invoke([&] { std::for_each(vector_.begin(), vector_.end(), f); }); }

    template <typename Func>
    void for_each(Func f) { invoker_.invoke([&] { std::for_each(vector_.begin(), vector_.end(), f); }); }

    iterator begin() { return invoker_.invoke_shared([this] { return iterator{vector_.begin(), invoker_}; }); }
    iterator end() { return invoker_.invoke_shared([this] { return iterator{vector_.end(), invoker_}; }); }
    const_iterator begin() const { return invoker_.invoke_shared([this] { return const_iterator{vector_.begin(), invoker_}; }); }
    const_iterator end() const { return invoker_.invoke_shared([this] { return const_iterator{vector_.end(), invoker_}; }); }
    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }

  private:
    UT ut_copy() const { return invoker_.invoke_shared([this] { return vector_; }); }
    UT ut_move() { return invoker_.invoke_shared([this] { return std::move(vector_); }); }
    SharedInvoker invoker_{};
    std::vector<T> vector_{};
};

} // namespace ax::atomic
