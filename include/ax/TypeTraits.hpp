//
// Created by Martin Miralles-Cordal on 8/21/19.
//

#pragma once

#include <functional>
#include <type_traits>

namespace ax {

/**
 * Conditionally adds or removes a const qualifier to or from a type.
 * @tparam Type The type to operate on
 * @tparam Const If true, add const. If false, remove const.
 */
template <typename Type, bool Const = true>
using const_t = typename std::conditional_t<Const, std::add_const_t<Type>, std::remove_const_t<Type>>;

/**
 * Conditionally adds or removes a const qualifier to or from a type, then evaluates to a pointer to that type.
 * @tparam Type The type to operate on.
 * @tparam Const If true, add const. If false, remove const.
 */
template <typename Type, bool Const = true>
using const_ptr_t = std::add_pointer_t<const_t<std::remove_reference_t<Type>, Const>>;

/**
 * Conditionally adds or removes a const qualifier to or from a type, then evaluates to a reference to that type.
 * @tparam Type The type to operate on.
 * @tparam Const If true, add const. If false, remove const.
 */
template <typename Type, bool Const = true>
using const_lvref_t = std::add_lvalue_reference_t<const_t<std::remove_reference_t<Type>, Const>>;

/**
 * The value type of T. (T minus all pointer and reference levels)
 * @tparam T The type to operate on.
 */
template <typename T>
using value_t = const_t<std::decay_t<T>, std::is_const<T>::value>;

/**
 * The lvalue reference type of T.
 * @tparam T The type to operate on.
 */
template <typename T>
using lvref_t = std::add_lvalue_reference_t<std::remove_reference_t<T>>;

/**
 * The non-const rvalue reference type of T.
 * @tparam T The type to operate on.
 */
template <typename T>
using rvref_t = std::add_rvalue_reference_t<std::remove_reference_t<std::remove_const_t<T>>>;

/**
 * The const lvalue reference type of T.
 * @tparam T The type to operate on.
 */
template <typename T> using cref_t = const_lvref_t<T, true>;

template <typename T, typename Func> constexpr inline bool returns_type = std::is_same<T, std::invoke_result_t<Func>>::value;
template<typename Func> constexpr inline bool returns_void = std::is_void<std::invoke_result_t<Func>>::value;
template <typename Func> constexpr inline bool returns_type<void, Func> = returns_void<Func>;

template <bool Cond, typename T = void> using if_t = typename std::enable_if<Cond, T>::type;

/**
 * Maps a sequence of given types to the given destination type.
 * Used to produce a dependent context for SFINAE to work.
 * @tparam Type The result type.
 * @tparam Ts The type sequence to evaluate in a dependent context.
 */
template<typename Type, typename...Ts> struct make_type { using type = Type; };
template<typename Type, typename...Ts> using make_type_t = typename make_type<Type, Ts...>::type;

/** Maps a type sequence to bool. */
template<typename...Ts> using bool_t = make_type_t<bool, Ts...>;

/** Maps a type sequence to void. Equivalent to C++17's std::void_t. */
template<typename...Ts> using void_t = make_type_t<void, Ts...>;

template <bool Value>
using bool_constant = std::integral_constant<bool, Value>;

template <bool Cond, typename Then, typename Else>
using ternary_t = typename std::conditional<Cond, Then, Else>::type;

template <typename...> struct all : std::true_type {};
template <typename B1> struct all<B1> : B1 {};
template <typename B1, typename...Bn> struct all<B1, Bn...> : ternary_t<bool(B1::value), all<Bn...>, B1> {};


template <typename...> struct any : std::false_type {};
template <typename B1> struct any<B1> : B1 {};
template <typename B1, typename...Bn> struct any<B1, Bn...> : ternary_t<bool(B1::value), B1, any<Bn...>> {};

template <class B> struct negation : std::integral_constant<bool, !bool(B::value)> {};

using std::is_same;

template <class T> struct identity { using type = T; };

/** Unconstructible dummy type to produce failures in template conditionals. */
struct nonesuch {
    nonesuch() = delete;
    ~nonesuch() = delete;
    nonesuch(nonesuch const&) = delete;
    void operator=(nonesuch const&) = delete;
};

template <class Default, class AlwaysVoid, template<class...> class Op, class... Args>
struct _detector : identity<Default> { using value_t = std::false_type; };

template <class Default, template<class...> class Op, class... Args>
struct _detector<Default, void_t<Op<Args...>>, Op, Args...> : identity<Op<Args...>> { using value_t = std::true_type; };

template <template<class...> class Op, class... Args>
using is_detected = typename _detector<nonesuch, void, Op, Args...>::value_t;

template <template<class...> class Op, class... Args>
using detected_t = typename _detector<nonesuch, void, Op, Args...>::type;

template <class Default, template<class...> class Op, class... Args>
using detected_or = _detector<Default, void, Op, Args...>;

template< template<class...> class Op, class... Args >
constexpr bool is_detected_v = is_detected<Op, Args...>::value;

template< class Default, template<class...> class Op, class... Args >
using detected_or_t = typename detected_or<Default, Op, Args...>::type;

template <class Expected, template<class...> class Op, class... Args>
using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;

template <class Expected, template<class...> class Op, class... Args>
constexpr bool is_detected_exact_v = is_detected_exact<Expected, Op, Args...>::value;

template <class To, template<class...> class Op, class... Args>
using is_detected_convertible = std::is_convertible<detected_t<Op, Args...>, To>;

template <class To, template<class...> class Op, class... Args>
constexpr bool is_detected_convertible_v = is_detected_convertible<To, Op, Args...>::value;

template <typename T, template <typename...> class... Ts> using meets_all_of = all<Ts<T>...>;
template <typename T, template <typename...> class... Ts> using meets_any_of = any<Ts<T>...>;
template <typename T, template <typename...> class... Ts> using meets_none_of = negation<any<Ts<T>...>>;
template <typename T, typename ...Ts> using all_same   = all<is_same<T, Ts>...>;
template <typename T, typename ...Ts> using is_any_of  = any<is_same<T, Ts>...>;
template <typename T, typename ...Ts> using is_none_of = negation<is_any_of<T, Ts...>>;
template <typename T, typename ...Ts> using any_are = is_any_of<T, Ts...>;
template <typename T, typename ...Ts> using none_are = is_none_of<T, Ts...>;

template <typename T, typename = void> struct iterator_type { using type = void; };
template <typename T> struct iterator_type<T, void_t<decltype(std::begin(std::declval<T>()))>> { using type = decltype(std::begin(std::declval<T>())); };
template <typename T> using iterator_type_t = typename iterator_type<T>::type;

template <typename T, typename = void> struct const_iterator_type { using type = void; };
template <typename T> struct const_iterator_type<T, void_t<decltype(std::cbegin(std::declval<T>()))>> { using type = decltype(std::cbegin(std::declval<T>())); };
template <typename T> using const_iterator_type_t = typename const_iterator_type<T>::type;

//template <typename T, typename = void> struct is_iterable : std::false_type {};
template <typename T> struct is_iterable : none_are<void, iterator_type_t<T>, const_iterator_type_t<T>> {};

template <typename T> using is_std_hashable = std::is_default_constructible<std::hash<T>>;

template <typename E>
using is_scoped_enum = std::integral_constant<bool, std::is_enum<E>::value && !std::is_convertible<E, int>::value>;

} // namespace ax
