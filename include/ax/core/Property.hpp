//
// Created by Martin Miralles-Cordal on 7/7/19.
//

#pragma once

#include "Lock.hpp"
#include "Signal.hpp"

namespace ax {
inline namespace core {

enum class InitType { Eager, Lazy };
enum class SyncType { Nonatomic, Atomic };

namespace internal {

template<typename T, SyncType S, typename Derived>
class PropertyBase;

} // namespace internal

template<typename T, SyncType S = SyncType::Nonatomic, InitType A = InitType::Eager>
class Property : public internal::PropertyBase<T, S, Property<T, S, A>>
{
  public:
    using internal::PropertyBase<T, S, Property>::operator=;

    T const &get() const
    {
        auto guard = internal::PropertyBase<T, S, Property>::lock_.Read.Guard();
        return internal::PropertyBase<T, S, Property>::value_;
    }

    explicit operator T const &() const { return get(); }

    Property &operator=(Property const &val) { set(val.get()); return *this; }
};


template<typename T, SyncType S>
class Property<T, S, InitType::Lazy> : public internal::PropertyBase<T, S, Property<T, S, InitType::Lazy>>
{
  public:
    using internal::PropertyBase<T, S, Property>::operator=;

    constexpr explicit Property(std::function<T()> generator) : generator_(std::move(generator)) {}

    T const &get() const
    {
        std::call_once(generatorFlag_, [this]() {
            auto guard = internal::PropertyBase<T, S, Property>::lock_.Write.guard();
            internal::PropertyBase<T, S, Property>::value_ = generator_();
        });

        auto guard = internal::PropertyBase<T, S, Property>::lock_.Read.guard();
        return internal::PropertyBase<T, S, Property>::value_;
    }

    explicit operator T const &() const { return get(); }

    Property &operator=(Property const &val) { set(val.get()); return *this; }

  private:
    std::function<T()> generator_;
    std::once_flag generatorFlag_;
};

namespace internal {

template<SyncType S>
class ReadWriteLock;

template<>
class ReadWriteLock<SyncType::Nonatomic>
{
  public:
    const DummyLock Read{};
    const DummyLock Write{};
};

template<>
class ReadWriteLock<SyncType::Atomic>
{
    mutable std::shared_mutex mutex_{};
  public:
    const Lock<std::shared_lock, decltype(mutex_)> Read{mutex_};
    const Lock<std::unique_lock, decltype(mutex_)> Write{mutex_};
};

template<typename T, SyncType S, typename Derived>
class PropertyBase
{
  protected:
    Signal<Derived const &, T const &> signal_;
    ReadWriteLock<S> lock_;
    T value_;

  public:
    void perform(std::function<void(T &)> const &mutator)
    {
        lock_.Write.lock();
        const auto oldValue = value_;
        mutator(value_);
        lock_.Write.unlock();
        if (oldValue != value_) {
            auto guard = lock_.Read.Guard();
            signal_.emit(*static_cast<Derived *>(this), oldValue);
        }
    }

    void set(T const &val)
    {
        lock_.Write.lock();
        const auto oldValue = value_;
        value_ = val;
        lock_.Write.unlock();
        auto guard = lock_.Read.guard();
        signal_.emit(*static_cast<Derived *>(this), oldValue);
    }

    PropertyBase &operator=(T const &val) { set(val); return *this; }

    int addObserver(typename decltype(signal_)::observer_type const &observer) const
    {
        return signal_.Connect(observer);
    }

    void removeObserver(int id) { signal_.Disconnect(id); }
};

} // namespace internal

}} // namespace ax::core
