//
// Created by Martin Miralles-Cordal on 8/13/19.
//

#pragma once

#include <type_traits>

namespace ax {

template <typename Iterator>
class Range
{
  public:
    using iterator = Iterator;
    constexpr Range(iterator b, iterator e) noexcept : begin_(b), end_(e) {}

    constexpr iterator begin() const noexcept { return begin_; }
    constexpr iterator end() const noexcept { return end_; }

    constexpr bool empty() const { return begin_ == end_; }

  private:
    iterator begin_;
    iterator end_;
};

template <typename Iterator>
class SizedRange
{
  public:
    using iterator = Iterator;
    constexpr SizedRange(iterator b, iterator e, std::size_t size) noexcept : range_{b, e}, size_(size) {}
    constexpr SizedRange(Range<Iterator> range, std::size_t size) noexcept : range_{range}, size_(size) {}

    constexpr iterator begin() const noexcept { return range_.begin(); }
    constexpr iterator end() const noexcept { return range_.end(); }
    [[nodiscard]] constexpr bool empty() const { return size_ == 0; }
    [[nodiscard]] constexpr std::size_t size() const noexcept { return size_; }

    constexpr operator Range<iterator> const &() const & noexcept { return range_; }

  private:
    Range<Iterator> range_;
    std::size_t size_;
};

template <typename Container>
using range_t = Range<decltype(std::declval<Container>().begin())>;

template <typename Container>
auto MakeRange(Container &container) -> range_t<Container>
{
    return range_t<Container>{container.begin(), container.end()};
}

} // namespace ax
