//
// Created by Martin Miralles-Cordal on 9/2/19.
//

#pragma once

#include <ax/Concepts.hpp>
#include <ax/core/Maybe.hpp>

namespace ax {

using PropertySetKey = std::string_view;

namespace concept {

GENERATE_METHOD_TRAIT(className);
GENERATE_METHOD_TRAIT(getProperty);
GENERATE_METHOD_TRAIT(hasProperty);
GENERATE_METHOD_TRAIT(getAllPropertyNames);

template<typename T, typename PropertyType>
CONCEPT PropertySet = require<
        CopyConstructible<T>,
        MoveConstructible<T>,
        Destructible<T>,
        converts_to<std::string, method::className, T>,
        converts_to<Maybe<PropertyType>, method::getProperty, T, PropertySetKey>,
        converts_to<PropertyType, ops::subscript, T, PropertySetKey>,
        identical_to<bool, method::hasProperty, T, PropertySetKey>,
        identical_to<std::vector<std::string>, method::getAllPropertyNames, T>
>;

GENERATE_METHOD_TRAIT(setProperty);
GENERATE_METHOD_TRAIT(clearProperty);
GENERATE_METHOD_TRAIT(clear);

template<typename T, typename PropertyType>
CONCEPT MutablePropertySet = require<
        concept::PropertySet<T, PropertyType>,
        exists<method::setProperty, T, PropertyType>,
        exists<method::clearProperty, T, PropertyType>,
        exists<method::clear, T>
>;

}


template<typename Value> class MutablePropertySet;

template<typename Value>
class PropertySet
{
  public:
    template <typename T>
    PropertySet(T impl) : impl_()
    {
        static_assert(concept::PropertySet<T, Value>);
        impl_ = std::make_shared<Model<T>>(std::move(impl));
    }
    /**
     * @brief  Named class of the property set.
     */
    [[nodiscard]] std::string className() const { return impl_->className(); }

    /**
     * Returns a property by name.
     * @param propertyName The name of the property to get.
     * @return The property value if found.
     */
    [[nodiscard]] Value operator[](PropertySetKey name) const { return (*impl_)[name]; }
    
    /**
     * Returns a property by name.
     * @param propertyName The name of the property to get.
     * @return The property value if found.
     */
    [[nodiscard]] Maybe<Value> getProperty(PropertySetKey name) const { return impl_->getProperty(name); }

    /** Convenience for quickly checking if the property is set at all if the client code is not interested in the value. */
    [[nodiscard]] bool hasProperty(PropertySetKey name) const { return impl_->hasProperty(name); }

    /** Get the list of names of all defined properties. */
    [[nodiscard]] std::vector<std::string> getAllPropertyNames() const { return impl_->getAllPropertyNames(); }

  private:
    friend class MutablePropertySet<Value>;
    struct Concept
    {
        virtual ~Concept() = default;
        [[nodiscard]] virtual std::string className() const = 0;
        [[nodiscard]] virtual Value operator[](PropertySetKey name) const = 0;
        [[nodiscard]] virtual Maybe<Value> getProperty(PropertySetKey name) const = 0;
        [[nodiscard]] virtual bool hasProperty(PropertySetKey name) const = 0;
        [[nodiscard]] virtual std::vector<std::string> getAllPropertyNames() const = 0;
    };

    template<typename T>
    struct Model : Concept
    {
        Model(T &&data) : data_(std::move(data)) {}
        [[nodiscard]] std::string className() const override { return data_.className(); }
        [[nodiscard]] Value operator[](PropertySetKey name) const override { return data_[name]; }
        [[nodiscard]] Maybe<Value> getProperty(PropertySetKey name) const override { return data_.getProperty(name); }
        [[nodiscard]] bool hasProperty(PropertySetKey name) const override { return data_.hasProperty(name); }
        [[nodiscard]] std::vector<std::string> getAllPropertyNames() const override { return data_.getAllPropertyNames(); }

        T data_;
    };

    std::shared_ptr<const Concept> impl_;
};

template<typename Value>
class MutablePropertySet
{
  public:
    template <typename T>
    MutablePropertySet(T impl) : impl_()
    {
        static_assert(concept::MutablePropertySet<T, Value>);
        impl_ = std::make_shared<Model<T>>(std::move(impl));
    }

    [[nodiscard]] std::string className() const { return impl_->className(); }
    [[nodiscard]] Value const& operator[](PropertySetKey name) const { return (*impl_)[name]; }
    [[nodiscard]] Maybe<Value> getProperty(PropertySetKey name) const { return impl_->getProperty(name); }
    [[nodiscard]] bool hasProperty(PropertySetKey name) const { return impl_->hasProperty(name); }
    [[nodiscard]] std::vector<std::string> getAllPropertyNames() const { return impl_->getAllPropertyNames(); }
    void setProperty(PropertySetKey key, Value value) { impl_->setProperty(key, value); }
    bool clearProperty(PropertySetKey key) { return impl_->clearProperty(key); }
    void clear() { impl_->clear(); }

  private:
    struct Concept : PropertySet<Value>::Concept
    {
        [[nodiscard]] virtual std::unique_ptr<Concept> copy() const = 0;
        virtual void setProperty(PropertySetKey key, Value value) = 0;
        virtual bool clearProperty(PropertySetKey key) = 0;
        virtual void clear() = 0;
    };

    template<typename T>
    struct Model : PropertySet<Value>::template Model<T>, Concept
    {
        using Super = typename PropertySet<Value>::template Model<T>;

        Model(T &&data) : Super(std::move(data)) {}
        [[nodiscard]] std::unique_ptr<Concept> copy() const override { return std::make_unique<Model<T>>(*this); }
        void setProperty(PropertySetKey key, Value value) override { Super::data_.setProperty(key, value); }
        bool clearProperty(PropertySetKey key) override { return Super::data_.clearProperty(key); }
        void clear() override { Super::data_.clear(); }
    };

    std::unique_ptr<Concept> impl_;
};

}