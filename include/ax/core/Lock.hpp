//
// Created by Martin Miralles-Cordal on 7/7/19.
//

#pragma once

#include <ax/SpinMutex.hpp>

#include <mutex>
#include <shared_mutex>

namespace ax {
inline namespace core {

template <typename L>
class [[nodiscard]] LockGuard
{
  public:
    constexpr LockGuard(const L &lock) : lock_(lock) { lock_.lock(); }
    ~LockGuard() { lock_.unlock(); }

  private:
    const L &lock_;
};

template <template<typename> class L, typename M>
class Lock {
  public:
    using mutex_type = M;
    constexpr Lock() = default;
    constexpr Lock(M &mutex) : lock_(mutex, std::defer_lock) {}
    constexpr void lock() const { lock_.lock(); }
    constexpr void unlock() const { lock_.unlock(); }
    mutex_type* mutex() const noexcept { return lock_.mutex(); }
    LockGuard<Lock> guard() const { return LockGuard{*this}; }

  private:
    mutable L<M> lock_;
};

template <typename M>
class NullLock
{
  public:
    using mutex_type = M;
    constexpr void lock() const {}
    constexpr void unlock() const {}
    constexpr mutex_type* mutex() const noexcept { return nullptr; }
    [[nodiscard]] constexpr bool owns_lock() const noexcept { return true; }
    constexpr explicit operator bool() const noexcept { return owns_lock(); }
};

class NullMutex
{
  public:
    void lock() const {}
    void unlock() const {}
    bool try_lock() const { return true; }
};

using SpinMutex = ax::SpinMutex;

using DummyLock = Lock<NullLock, NullMutex>;

}} // namespace ax::core
