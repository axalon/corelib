//
// Created by Martin Miralles-Cordal on 7/29/19.
//

#pragma once

#include <cassert>
#include <memory>
#include <typeindex>
#include <type_traits>

namespace ax {
inline namespace core {

namespace detail::TaggedUnion {
template <typename... Ts> struct Helper;
}

/**
 * A union which keeps track of the stored type. It reserves storage space large enough to store any one of the provided
 * template parameter types.
 *
 * TaggedUnion is only partially type-safe! It will manage the proper construction and destruction of the contained
 * value, as well as assert() that the type you request is the type that is actually in storage, but it does not protect
 * you from setting the value to a type outside the template parameter list (or larger than its allocated storage!) or
 * protect you from getting the wrong type when assertions are disabled. Because of this, it should *NOT* be used
 * directly. It is meant to be used as the backing storage for other storage types, and it is expected that those types
 * will take care of all their respective type safety concerns.
 *
 * @tparam F The first type. At least one type is required, otherwise, what are we doing here?
 * @tparam Rest The rest of the types to consider in storage sizing.
 */
template <typename F, typename... Rest>
class TaggedUnion
{
  public:
    template <typename... Ts> using Helper =  detail::TaggedUnion::Helper<Ts...>;
    TaggedUnion() = default;
    TaggedUnion(TaggedUnion const &other) : storage_{}, currentType_{other.currentType_}
    {
        Helper<F, Rest...>::copy(other.currentType_, std::addressof(other.storage_), std::addressof(storage_));
    }

    TaggedUnion(TaggedUnion &&other) : storage_{}, currentType_{other.currentType_}
    {
        Helper<F, Rest...>::move(other.currentType_, std::addressof(other.storage_), std::addressof(storage_));
        other.currentType_ = 0u;
    }

    TaggedUnion &operator=(TaggedUnion const &rhs)
    {
        if (currentType_) { Helper<F, Rest...>::destroy(currentType_, std::addressof(storage_)); }
        Helper<F, Rest...>::copy(rhs.currentType_, std::addressof(rhs.storage_), std::addressof(storage_));
        currentType_ = rhs.currentType_;
        return *this;
    }

    TaggedUnion &operator=(TaggedUnion &&rhs)
    {
        if (currentType_) { Helper<F, Rest...>::destroy(currentType_, std::addressof(storage_)); }
        Helper<F, Rest...>::move(rhs.currentType_, std::addressof(rhs.storage_), std::addressof(storage_));
        currentType_ = rhs.currentType_;
        rhs.currentType_ = 0u;
        return *this;
    }

    template <typename T, typename Type = typename std::decay<T>::type>
    TaggedUnion(T &&value)
    {
        new (std::addressof(storage_)) Type{std::forward<T>(value)};
        currentType_ = typeid(Type).hash_code();
    }

    ~TaggedUnion() { if (currentType_) { Helper<F, Rest...>::destroy(currentType_, std::addressof(storage_)); } }

    template <typename T, typename Type = typename std::decay<T>::type>
    Type &get()
    {
        assert(currentType_ == typeid(Type).hash_code());
        return *reinterpret_cast<Type*>(&storage_);
    }

    template <typename T, typename Type = typename std::decay<T>::type>
    Type const &get() const
    {
        assert(currentType_ == typeid(Type).hash_code());
        return *reinterpret_cast<Type const *>(&storage_);
    }

    template <typename T>
    void set(T &&value)
    {
        using Type = typename std::decay<T>::type;
        if (currentType_) { Helper<F, Rest...>::destroy(currentType_, std::addressof(storage_)); }
        new (std::addressof(storage_)) Type{std::forward<T>(value)};
        currentType_ = typeid(Type).hash_code();
    }

    template <typename T>
    [[nodiscard]] bool isType() const noexcept { return isType(std::type_index{typeid(T)}); }
    [[nodiscard]] bool isType(std::type_index type) const noexcept { return isType(type.hash_code()); }
    [[nodiscard]] bool isType(size_t hash) const noexcept { return currentType_ == hash; }

  private:
    typename std::aligned_union<sizeof(F), Rest...>::type storage_;
    size_t currentType_{0};
};
namespace detail::TaggedUnion {

template <typename F, typename... Rest>
struct Helper<F, Rest...>
{
    inline static void copy(size_t id, void const *data, void *dest)
    {
        if (id == typeid(F).hash_code()) { new (dest) F{*static_cast<const F*>(data)}; }
        else { Helper<Rest...>::copy(id, data, dest); }
    }

    inline static void move(size_t id, void *data, void *dest)
    {
        if (id == typeid(F).hash_code()) { new (dest) F{std::move(*static_cast<F*>(data))}; }
        else { Helper<Rest...>::move(id, data, dest); }
    }

    inline static void destroy(size_t id, void *data)
    {
        if (id == typeid(F).hash_code()) { (*static_cast<F*>(data)).~F(); }
        else { Helper<Rest...>::destroy(id, data); }
    }
};

template<>
struct Helper<>
{
    inline static void copy(size_t, void const *, void *) {}
    inline static void move(size_t, void *, void *) {}
    inline static void destroy(size_t, void *) { }
};

} // namespace detail::TaggedUnion

}} // namespace ax::core
