//
// Created by Martin Miralles-Cordal on 8/13/19.
//

#pragma once

#include <unordered_map>

namespace ax {
inline namespace core {

template <typename K, typename V, typename H = std::hash<K>>
class ThreadLocalCache
{
  public:
    using Cache = std::unordered_map<K, V, H>;
    using Key = K;
    using Value = V;
    using HashFunction = H;

    decltype(auto) operator[](K const &key) { return cache()[key]; }
    decltype(auto) operator[](K &&key) { return cache()[std::move(key)]; }

    decltype(auto) get(K const &key) const { return cache().at(key); }

    template<typename K2, typename...Args>
    void emplace(K2 &&keyv, Args&&... args)
    {
        if (cache().count(Key{keyv}) == 0) {
            cache().emplace(std::forward<K2>(keyv), std::forward<Args>(args)...);
        }
    }

    void clear()
    {
        for (auto &cache : caches_) { cache->clear(); }
    }

  private:
    class CacheHandle
    {
      public:
        CacheHandle() = default;
        CacheHandle(ThreadLocalCache const *tl, Cache *cache) : parent_(tl), cache_(cache) {}
        ~CacheHandle() { if (parent_ && cache_) { parent_->caches_.erase(cache_); } }
        operator Cache *() { return cache_; }
        operator Cache *() const { return cache_; }
      private:
        ThreadLocalCache const *parent_ = nullptr;
        Cache *cache_ = nullptr;
    };

    using TLCaches = std::unordered_map<ThreadLocalCache const *, CacheHandle>;

    Cache &cache() const
    {
        if (threadLocalCaches().count(this) == 0) {
            auto newCache = std::make_unique<Cache>();
            auto newCacheValue = newCache.get();
            threadLocalCaches().emplace(this, CacheHandle{this, newCacheValue});
            caches_.emplace(newCacheValue, std::move(newCache));
            return *newCacheValue;
        } else {
            return *threadLocalCaches()[this];
        }
    }


    static TLCaches& threadLocalCaches()
    {
        static thread_local TLCaches s_threadLocalCaches{};
        return s_threadLocalCaches;
    }

    mutable std::unordered_map<Cache *, std::unique_ptr<Cache>> caches_;
};

}} // namespace ax::core
