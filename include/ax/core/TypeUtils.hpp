//
// Created by Martin Miralles-Cordal on 8/3/19.
//

#pragma once

#include <ax/Concepts.hpp>
#include <ax/Defines.hpp>
#include <ax/TypeTraits.hpp>

#include <cassert>
#include <tuple>

namespace ax {
inline namespace core {

namespace Sink {

namespace _internal {
struct _unused;
template<typename T, bool Cond> using _copyBase = std::conditional_t<Cond, std::decay_t<T>, _unused>;
template<typename T, bool Cond> using Copy = std::conditional_t<std::is_reference<T>::value, _copyBase<T, Cond>, _copyBase<T, Cond> const> &;
template<typename T, bool Cond> using Move = std::conditional_t<Cond, std::decay_t<T>, _unused> &&;
}

/**
 * Enables if the type matches the desired type. Otherwise, substitution fails.
 *
 * @note Be sure to ask yourself if you even need to use this and if you can simply use a non-template overload instead.
 */
template<typename Wanted, typename T>
using Matches = std::conditional_t<std::is_same<Wanted, T>::value, T, _internal::_unused>;

template<typename T, bool...Bs>
using Conforms = std::conditional_t<require<Bs...>, T, _internal::_unused>;

/** Enables if the type does not match the unwanted type. Otherwise, substitution fails. */
template<typename Unwanted, typename T>
using Not = std::conditional_t<!std::is_same<Unwanted, T>::value, T, _internal::_unused>;

/** The appropriate type for copying T, if T is copy constructible and assignable. Otherwise, substitution fails. */
template<typename T> using Copy = _internal::Copy<T, concept::CopyMutable<T>>;
/** The appropriate type for moving T, if T is move constructible and assignable. Otherwise, substitution fails. */
template<typename T> using Move = _internal::Move<T, concept::MoveMutable<T>>;

/**
 * Sink parameters for T constructors.
 */
namespace Construct {
/** The appropriate type for copy constructing with T, if T is copy constructible. Otherwise, substitution fails. */
template<typename T> using Copy = _internal::Copy<T, concept::CopyConstructible<T>>;
/** The appropriate type for move constructing with T, if T is move constructible. Otherwise, substitution fails. */
template<typename T> using Move = _internal::Move<T, concept::MoveConstructible<T>>;
}

/**
 * Sink parameters for T assignment operations.
 */
namespace Assign {
/** The appropriate type for copy assigning with T, if T is copy assignable. Otherwise, substitution fails. */
template<typename T> using Copy = _internal::Copy<T, concept::CopyAssignable<T>>;
/** The appropriate type for move assigning with T, if T is move assignable. Otherwise, substitution fails. */
template<typename T> using Move = _internal::Move<T, concept::MoveAssignable<T>>;
}

} // namespace Sink

namespace detail {
template<typename F, typename Tuple, std::size_t ... I>
constexpr decltype(auto) apply_impl(F &&f, Tuple &&t, std::index_sequence<I...>)
{
    return static_cast<F &&>(f)(std::get<I>(static_cast<Tuple &&>(t)) ...);
}
}

// Implementation of a simplified std::apply from C++17
template <typename F, typename Tuple>
constexpr decltype(auto) Apply(F&& f, Tuple&& t)
{
    return detail::apply_impl(static_cast<F&&>(f), static_cast<Tuple&&>(t),
                              std::make_index_sequence<std::tuple_size<std::remove_reference_t<Tuple>>::value>{});
}

template <typename F, typename...Args>
constexpr decltype(auto) Invocation(F&& f, Args&&... args)
{
    return [f=std::forward<F>(f), args=std::tuple<Args...>{std::forward<Args>(args)...}] {
        return Apply(f, std::move(args));
    };
}

}} // namespace ax::core
