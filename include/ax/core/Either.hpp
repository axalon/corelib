//
// Created by Martin Miralles-Cordal on 7/1/19.
//

#pragma once

#include "Maybe.hpp"

namespace ax {
inline namespace core {

template <typename L, typename R>
class Either
{
  public:
    using left_type = L;
    using right_type = R;
    using value_type = right_type;

    constexpr Either() : store_{}, state_(Neither) {}
    ~Either() { store_.destroy(state_); }

    constexpr Either(const Either &other) : store_{}, state_(other.state_)
    {
        switch (state_)
        {
            case Neither: break;
            case Left: store_.init(other.store_.left_); break;
            case Right: store_.init(other.store_.right_); break;
        }
    }

    constexpr Either(Either &&other) noexcept : store_{}, state_(other.state_)
    {
        switch (state_)
        {
            case Neither: break;
            case Left: store_.init(std::move(other.store_.left_)); break;
            case Right: store_.init(std::move(other.store_.right_)); break;
        }
    }

    explicit constexpr Either(Sink::Construct::Copy<L> left) : store_(left), state_(Left) {}
    constexpr Either &operator=(Sink::Assign::Copy<L> value) { set_<L>(value); return *this; }

    explicit constexpr Either(Sink::Construct::Move<L> left) : store_(std::move(left)), state_(Left) {}
    constexpr Either &operator=(Sink::Assign::Move<L> value) { move_<L>(std::move(value)); return *this; }

    explicit constexpr Either(Sink::Construct::Copy<R> right) : store_(right), state_(Right) {}
    constexpr Either &operator=(Sink::Assign::Copy<R> value) { set_<R>(value); return *this; }

    explicit constexpr Either(Sink::Construct::Move<R> right) : store_(std::move(right)), state_(Right) {}
    constexpr Either &operator=(Sink::Assign::Move<R> value) { move_<R>(std::move(value)); return *this; }

    /** Copy assignment operator */
    Either &operator=(const Either &other)
    {
        if (state_ != other.state_)
        {
            store_.Destroy(state_);
            state_ = other.state_;
            switch (other.state_)
            {
                case Neither: break;
                case Left: store_.Init(other.store_.left_); break;
                case Right: store_.Init(other.store_.right_); break;
            }
        }
        else
        {
            switch (other.state_)
            {
               case Neither: break;
               case Left: store_.left_ = other.store_.left_; break;
               case Right: store_.right_ = other.store_.right_; break;
            }
        }

        return *this;
    }

    /** Move assignment operator */
    Either &operator=(Either &&other) noexcept
    {
        if (state_ != other.state_)
        {
            store_.Destroy(state_);
            state_ = other.state_;
            switch (other.state_)
            {
                case Neither: break;
                case Left: store_.Init(std::move(other.store_.left_)); break;
                case Right: store_.Init(std::move(other.store_.right_)); break;
            }
        }
        else
        {
            switch (other.state_)
            {
                case Neither: break;
                case Left: store_.left_ = std::move(other.store_.left_); break;
                case Right: store_.right_ = std::move(other.store_.right_); break;
            }
        }

        return *this;
    }

    /** Returns whether the left value is set. */
    constexpr bool isLeft() const { return state_ == Left; }

    /** Returns whether the right value is set. */
    constexpr bool isRight() const { return state_ == Right; }

    /**
     * Returns the left value.
     * @return A const ref to the value. Return value is undefined if not set.
     */
    constexpr L const & left() const { return store_.left_; }

    /**
     * Returns the left value.
     * @return A reference to the value. Return value is undefined if not set.
     */
    constexpr L & left() { return store_.left_; }

    /**
     * Returns the right value.
     * @return A const ref to the value. Return value is undefined if not set.
     */
    constexpr R const & right() const { return store_.right_; }

    /**
     * Returns the right value.
     * @return A reference to the value. Return value is undefined if not set.
     */
    constexpr R & right() { return store_.right_; }

    /**
     * Returns a Maybe for the left value.
     * @return A Maybe that is set if the value exists or empty otherwise.
     */
    constexpr Maybe<L> maybeLeft() { return !isRight() ? store_.left_ : Nothing{}; }

    /**
     * Returns a Maybe for the right value.
     * @return A Maybe that is set if the value exists or empty otherwise.
     */
    constexpr Maybe<R> maybeRight() { return isRight() ? store_.right_ : Nothing{}; }

    /**
     * Returns a copy of the left value, or a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr L leftOr(L &&other) const &
    {
        return !isRight() ? store_.left_ : std::forward<L>(other);
    }

    /**
     * Moves out the left value or returns a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr L leftOr(L &&other) &&
    {
        return !isRight() ? std::move(store_.left_) : std::forward<L>(other);
    }

    /**
     * Returns a copy of the right value, or a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr R rightOr(R &&other) const &
    {
        return isRight() ? store_.right_ : std::forward<L>(other);
    }

    /**
     * Moves out the right value or returns a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr R rightOr(R &&other) &&
    {
        return isRight() ? std::move(store_.right_) : std::forward<L>(other);
    }

    /**
     * Monadic bind operator.
     * @param func A function that takes an R and returns an Either<L, Rp>.
     * @return The result of the function if right is set and left otherwise.
     */
    template <typename Func>
    auto operator|(Func func) -> decltype(func(R{}))
    {
        return _bind_internal<typename decltype(func(this->right()))::right_type>(func);
    }

    /**
     * An "otherwise" bind.
     * @param func The function to run which returns an Either of the same type.
     * @return The result of the function if left is set and a copy of itself otherwise.
     */
    Either operator|(std::function<Either(L)> func)
    {
        if (isLeft()) {
            return func(left());
        } else {
            return *this;
        }
    }

  private:
    enum State { Neither = 0, Left = 1, Right = 2 };

    template <typename T>
    static constexpr State kStateFor = MatchesAny<T, L, R> ? (SameAs<T, R> ? Left : Right) : Neither;

    template<typename T> using storage_type = std::conditional_t<
            std::is_reference_v<T>,
            std::reference_wrapper<std::remove_reference_t<T>>,
            T>;

    template<typename Ls, typename Rs>
    union Storage
    {
        Storage() : neither_() {}
        Nothing neither_;
        ~Storage() {}

        template <typename T> void destroy_(T &val) { val.~T(); }

        #define Storage_MEMBER_IMPL(_tp, _name) \
        explicit Storage(Sink::Construct::Copy<_tp> aValue) : _name##_(aValue) {} \
        explicit Storage(Sink::Construct::Move<_tp> aValue) : _name##_(std::move(aValue)) {} \
        void set(Sink::Assign::Copy<_tp> val) { _name##_ = val; } \
        void set(Sink::Assign::Move<_tp> val) { _name##_ = std::move(val); } \
        void init(Sink::Construct::Copy<_tp> val) { new(std::addressof(_name##_)) storage_type<_tp>{val}; } \
        void init(Sink::Construct::Move<_tp> val) { new(std::addressof(_name##_)) storage_type<_tp>{std::move(val)}; } \
        storage_type<_tp> _name##_

        Storage_MEMBER_IMPL(Ls, left);
        Storage_MEMBER_IMPL(Rs, right);

        void destroy(State state)
        {
            switch (state)
            {
                case Left: left_.~Ls(); break;
                case Right: right_.~Rs(); break;
                case Neither: break;
            }
        }

        #undef Storage_MEMBER_IMPL
    };

    template <typename T>
    constexpr void set_(const_lvref_t<T> value)
    {
        if (state_ != kStateFor<T>)
        {
            store_.destroy(state_);
            store_.init(value);
        }
        else
        {
            store_.set(value);
        }

        state_ = kStateFor<T>;
    }

    template <typename T>
    constexpr void move_(rvref_t<T> value)
    {
        if (state_ != kStateFor<T>)
        {
            store_.destroy(state_);
            store_.init(std::move(value));
        }
        else
        {
            store_.set(std::move(value));
        }

        state_ = kStateFor<T>;
    }

    Storage<std::remove_const_t<L>, std::remove_const_t<R>> store_;
    State state_{Neither};

    template <typename Tp>
    constexpr Either<L, Tp> _bind_internal(std::function<Either<L, Tp>(R)> func)
    {
        if (isRight()) {
            return func(store_.right_);
        } else {
            return Either<L, Tp>{store_.left_};
        }
    }
};

}} // namespace ax::core
