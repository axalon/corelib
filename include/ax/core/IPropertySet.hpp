//
// Created by Martin Miralles-Cordal on 9/2/19.
//

#pragma once


/** A named set of named properties. */
class IPropertySet
{
  public:
    virtual ~IPropertySet()
    {}

    /**
     * @brief  Named class of the property set.
     */
    virtual const std::string& Class() const = 0;

    /**
     * Returns a property by name.
     * @param propertyName The name of the property to get.
     * @return The property value if found.
     */
    virtual GetPropertyOutcome GetProperty(const std::string& propertyName) const = 0;
    /**
     * Try getting a property by name. If property does not exist, an empty Optional is returned, not an error.
     * @param propertyName The name of the property to get.
     * @return The property value if found.
     */
    virtual core::Outcome<core::Optional<Property>> TryGetProperty(const std::string& propertyName) const = 0;

    /** Convenience for quickly checking if the property is set at all if the client code is not interested in the value. */
    virtual bool HasProperty(const std::string& propertyName) const = 0;

    /** Set a property by name. */
    virtual EmptyOutcome SetProperty(const std::string& propertyName, const Property& value) = 0;
    virtual EmptyOutcome SetProperty(const std::string& propertyName, Property&& value) = 0;

    /** Removes a property by name from the set. */
    virtual EmptyOutcome UnsetProperty(const std::string& propertyName) = 0;

    /** Get the list of names of all defined properties. */
    virtual  Outcome<std::vector<std::string>> GetAllPropertyNames() const = 0;

    /** Creates a deep copy copy of the property set. */
    virtual Outcome<UniquePtr>
    ClonePropertySet(IThreadAccessibilityPolicyPtr threadAccessibilityPolicy = ANY_THREAD_POLICY) const = 0;

    /**
     * @brief If the IPropertySet supports normalized relations, this will return a vector of related IPropertySets.
     * Otherwise, it will return an error.
     * @return Vector of related property sets or Error_NormalizedRelationsNotSupported if not supported.
     */
    virtual core::Outcome<std::vector<SharedPtr>> GetRelatedPropertySets(const std::string& relationId) const = 0;

    /**
     * @brief IPropertySet capabilities can be queried using this API.
     * @return Set of supported capabilities.
     */
    virtual std::set<OptionalCapability> GetOptionalCapabilities() const = 0;

    /**
     * @brief Convenience function for checking if an optional capability is supported.
     * @param capability The capability we want to check is supported.
     * @return A bool indicating whether the capability is supported.
     */
    virtual bool HasOptionalCapability(OptionalCapability capability) const = 0;

    virtual Hash::Value Hash() const = 0;
};
