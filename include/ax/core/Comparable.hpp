//
// Created by Martin Miralles-Cordal on 8/3/19.
//

#pragma once

namespace ax {
inline namespace core {

namespace detail {

/** L != R */
template<typename L, typename R>
struct _Equatable { constexpr friend bool operator!=(L const &l, R const &r) { return !(l == r); } };

/** L <= R, L >= R, L > R */
template<typename L, typename R>
struct _Comparable : _Equatable<L, R>
{
    constexpr friend bool operator<=(L const &l, R const &r) { return (l == r) || (l < r); }
    constexpr friend bool operator>=(L const &l, R const &r) { return !(l < r); }
    constexpr friend bool operator>(L const &l, R const &r) { return (l != r) && !(l < r); }
};

}


/**
 * Implements 'L != R', 'R == L', and 'R != L' from 'L == R'.
 * @tparam L Left-hand side type.
 * @tparam R Right-hand side type.
 */
template<typename L, typename R>
struct Equatable : detail::_Equatable<L, R>, detail::_Equatable<R, L>
{
    constexpr friend bool operator==(R const &l, L const &r) { return r == l; }
};


/**
 * Implements 'L != L'.
 * @tparam L The type to compare values of.
 */
template<typename L>
struct Equatable<L, L> : detail::_Equatable<L, L> {};

/**
 * Defines and implements all other comparison operators for (L # R) and (R # L) in terms of 'L == R' and 'L < R'.
 * @tparam L Left-hand side type.
 * @tparam R Right-hand side type.
 */
template<typename L, typename R = L>
struct Comparable : detail::_Comparable<L, R>, detail::_Comparable<R, L>
{
    constexpr friend bool operator==(R const &l, L const &r) { return r == l; }
    constexpr friend bool operator<(R const &l, L const &r) { return r >= l; }
};

/**
 * Defines and implements all other comparison operators for (L # L) in terms of 'L == L' and 'L < L'.
 * @tparam L The type to compare values of.
 */
template<typename L>
struct Comparable<L, L> : detail::_Comparable<L, L> {};
}
} // namespace ax::core
