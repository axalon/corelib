//
// Created by Martin Miralles-Cordal on 7/6/19.
//

#pragma once

#include <functional>
#include <map>

namespace ax {
inline namespace core {

template<typename... Args>
class Signal
{
  public:
    using observer_type = std::function<void(Args...)>;
    struct Token;

    Signal() : currentId_(0) {}
    Signal(Signal const &other) = delete;
    Signal(Signal &&other) = default;

    // connects a member function to this Signal
    template<typename T>
    [[nodiscard]] Token connectMember(T *inst, void (T::*func)(Args...))
    {
        return Connect([=](Args... args) {
            (inst->*func)(args...);
        });
    }

    // connects a const member function to this Signal
    template<typename T>
    [[nodiscard]] Token connectMember(T *inst, void (T::*func)(Args...) const)
    {
        return connect([=](Args... args) {
            (inst->*func)(args...);
        });
    }

    // connects a std::function to the signal. The returned
    // value can be used to disconnect the function again
    [[nodiscard]] Token connect(std::function<void(Args...)> const &slot)
    {
        slots_.insert(std::make_pair(++currentId_, slot));
        return Token{*this, currentId_};
    }

    // disconnects all previously connected functions
    void disconnectAll()
    {
        slots_.clear();
    }

    // calls all connected functions
    void emit(Args... p)
    {
        for (auto it : slots_) {
            it.second(p...);
        }
    }

  private:
    // disconnects a previously connected function
    void disconnect(Token &id)
    {
        slots_.erase(id.slot_);
        id.disown();
    }

    std::map<int, std::function<void(Args...)>> slots_;
    int currentId_{0};
};

template<typename... Args>
struct Signal<Args...>::Token
{
    constexpr Token() = default;

    constexpr Token(Token &&other) : signal_(other.signal_), slot_(other.slot_) { other.disown(); }
    constexpr Token& operator=(Token &&other)
    {
        disconnect();
        signal_ = other.signal_; const_cast<int&>(slot_) = other.slot_;
        other.disown();
        return *this;
    }

    ~Token() { disconnect(); }

    constexpr inline void disconnect() { if (signal_) { signal_->disconnect(*this); disown(); } }

    constexpr inline bool valid() const { return signal_ != nullptr; }

  private:
    friend class Signal;

    constexpr Token(Signal &signal, int slot) : signal_{&signal}, slot_{slot} {}
    constexpr inline void disown() { signal_ = nullptr; }

    Signal *signal_ = nullptr;
    int const slot_ = -1;
};

}} // namespace ax::core
