//
// Created by Martin Miralles-Cordal on 7/14/19.
//

#pragma once

#include "Comparable.hpp"
#include "Signal.hpp"

namespace ax {
inline namespace core {

template <typename Alias, typename T>
struct Wrapper : Comparable<Wrapper<Alias,T>>, Comparable<Wrapper<Alias,T>, T>
{
    T value;

    constexpr Wrapper() = default;

    constexpr Wrapper(Wrapper const &rhs) = default;
    constexpr Wrapper(Wrapper &&rhs) = default;

    explicit constexpr Wrapper(T const &argValue) : value(argValue) {}
    explicit constexpr Wrapper(T &&argValue) : value(std::move(argValue)) {}

    constexpr operator T const &() const { return value; }
    constexpr operator T&() { return value; }

    constexpr Wrapper &operator=(Wrapper const &rhs) = default;
    constexpr Wrapper &operator=(Wrapper &&rhs) = default;

    constexpr Wrapper &operator=(T const &rhs) { value = rhs; return *this; }
    constexpr Wrapper &operator=(T &&rhs) { value = std::move(rhs); return *this; }

    constexpr friend bool operator==(Wrapper const &lhs, Wrapper const &rhs) { return lhs.value == rhs.value; }
    constexpr friend bool operator<(Wrapper const &lhs, Wrapper const &rhs) { return lhs.value < rhs.value; }

    constexpr friend bool operator==(Wrapper const &lhs, T const &rhs) { return lhs.value == rhs; }
    constexpr friend bool operator<(Wrapper const &lhs, T const &rhs) { return lhs.value < rhs; }
};

class OnDestroy
{
  public:
    virtual ~OnDestroy() { onDestroy_.emit(); }

    int watch(std::function<void()> const &slot) { return onDestroy_.connect(slot); }
    void unwatch(int slot) { onDestroy_.disconnect(slot); }

  private:
    Signal<> onDestroy_;
};

}} // namespace ax::core
