//
// Created by Martin Miralles-Cordal on 7/1/19.
//

#pragma once

#include "TaggedUnion.hpp"
#include "TypeUtils.hpp"

#include <functional>

namespace ax {
inline namespace core {

class Nothing{};

template <typename T>
class Maybe
{
  public:
    template <bool Const> struct BasicIterator;
    using iterator = BasicIterator<false>;
    using const_iterator = BasicIterator<true>;
    using value_type = typename std::remove_reference<T>::type;
    using mutable_type = typename std::remove_const<T>::type;
    using reference = value_type&;
    using const_reference = const value_type&;
    using rvalue_reference = value_type&&;
    using pointer_type = value_type *;
    using size_type = std::size_t;
    using difference_type = size_type;

    constexpr Maybe() : _store(Nothing{}) {}
    constexpr Maybe(Sink::Not<T, Nothing>) : Maybe() {}
    constexpr Maybe(Sink::Construct::Copy<T> v) : _store() { _store.update(v); }
    constexpr Maybe &operator=(Sink::Construct::Copy<T> rhs) = delete;//{ _store.update(rhs); return *this; }
    constexpr Maybe(Sink::Construct::Move<T> v) : _store() { _store.update(std::move(v)); }
    constexpr Maybe &operator=(Sink::Construct::Move<T> rhs) = delete;//{ _store.update(std::move(rhs)); return *this; }

    constexpr Maybe(Maybe const &other) : _store(other._store) {}
    constexpr Maybe &operator=(Maybe const &rhs) = delete;//{ _store = rhs._store; return *this; }
    constexpr Maybe(Maybe &&other) : _store(std::move(other._store)) {}
    constexpr Maybe &operator=(Maybe &&rhs) = delete;//{ _store = std::move(rhs._store); return *this; }

    /** Returns whether this contains a value. */
    constexpr explicit operator bool() const noexcept { return isSet(); }

    /** Returns whether this contains a value. */
    [[nodiscard]] constexpr bool isSet() const noexcept { return !_store.isType(typeid(Nothing).hash_code()); }

    /**
     * Accesses the contained value.
     * @return A const pointer to the value if set, nullptr otherwise.
     */
    constexpr value_type const * operator->() const { return isSet() ? &_store.value() : nullptr; }

    /**
     * Accesses the contained value.
     * @return A pointer to the value if set, nullptr otherwise.
     */
    constexpr value_type * operator->() { return isSet() ? &_store.value() : nullptr; }

    /**
     * Accesses the contained value.
     * @return A const ref to the value. Return value is undefined if not set.
     */
    constexpr value_type const & operator*() const { return _store.value(); }

    /**
     * Accesses the contained value.
     * @return A reference to the value. Return value is undefined if not set.
     */
    constexpr value_type & operator*() { return _store.value(); }

    /**
     * Returns the contained value.
     * @return A const ref to the value. Return value is undefined if not set.
     */
    constexpr value_type const & get() const { return _store.value(); }

    /**
     * Returns the contained value.
     * @return A reference to the value. Return value is undefined if not set.
     */
    constexpr value_type & get() { return _store.value(); }

    /**
     * Returns a copy of the contained value, or a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr value_type getOr(T &&other) const &
    {
        return isSet() ? _store.value() : std::forward<T>(other);
    }

    /**
     * Moves out the contained value or returns a default value if not set.
     * @param other The value to default to.
     * @return A copy of the value, or the default value if this is not set.
     */
    constexpr value_type getOr(T &&other) &&
    {
        return isSet() ? std::move(_store.value()) : std::forward<T>(other);
    }

    /**
     * Monadic bind operator.
     * @param func A function with one of the following signatures:
     *             1. "func(T&&) -> Maybe<U>". Called when this Maybe is set.
     *             2. "func(T&&) -> void". Called when this Maybe is set.
     * @return If 1: The result of the function if the value is set and an empty Maybe<U> otherwise.
     *         If 2: A move-constructed copy of `this`.
     */
    template<typename Func>
    auto operator&(Func func) &&
    {
        if constexpr (std::is_invocable_v<Func, rvalue_reference>) {
            using Result = std::invoke_result_t<Func, rvalue_reference>;
            if constexpr (std::is_same_v<void, Result>) {
                if (isSet()) { func(std::move(_store.value())); }
                return std::move(*this);
            } else {
                if (isSet()) { return func(std::move(_store.value())); }
                else { return Result{Nothing{}}; }
            }
        } else {
            static_assert(!std::is_same_v<Func, Func>,
                          "Maybe::operator& (\"WithValue\") passed an incompatible callable.");
        }
    }

    /**
     * Monadic bind operator.
     * @param func A function with one of the following signatures:
     *             1. "func(T const&) -> Maybe<U>". Called when this Maybe is set.
     *             2. "func(T const&) -> void". Called when this Maybe is set.
     * @return If 1: The result of the function if the value is set and an empty Maybe<U> otherwise.
     *         If 2: A move-constructed copy of `this`.
     */
    template<typename Func>
    auto operator&(Func func) const &
    {
        if constexpr (std::is_invocable_v<Func, const_reference>) {
            using Result = std::invoke_result_t<Func, const_reference>;
            if constexpr (std::is_same_v<void, Result>) {
                if (isSet()) { func(get()); }
                return std::move(*this);
            } else {
                if (isSet()) { return func(get()); }
                else { return Result{Nothing{}}; }
            }
        } else {
            static_assert(!std::is_same_v<Func, Func>,
                          "Maybe::operator& (\"WithValue\") passed an incompatible callable.");
        }
    }

    template<typename Func> auto operator>>(Func func) && { return operator&(func); }
    template<typename Func> auto operator>>(Func func) const & { return operator&(func); }

    /**                                                                                                                   
     * An "otherwise" bind.
     * @param func A function with the signature "func() -> Maybe<T>". Called when this Maybe is empty.
     * @return The result of the function if the value is not set and a move-constructed copy of `this` otherwise.
     */
    template<typename Func>
    auto operator|(Func func) &&
    {
        if constexpr (std::is_invocable_r_v<Maybe, Func>) {
            static_assert(std::is_same_v<Maybe, std::invoke_result_t<Func>>,
                          "Maybe::operator| (\"otherwise\") passed a function that does not return Maybe<T>.");
            if (!isSet()) { return func(); }
            else { return std::move(*this); }
        } else {
            static_assert(!std::is_same_v<Func, Func>,
                          "Maybe::operator| (\"otherwise\") passed an incompatible callable.");
        }
    }

    /**
     * An "otherwise" bind.
     * @param func A function with the signature "func() -> Maybe<T>". Called when this Maybe is empty.
     * @return The result of the function if the value is not set and a move-constructed copy of `this` otherwise.
     */
    template<typename Func>
    auto operator|(Func func) const &
    {
        if constexpr (std::is_invocable_r_v<Maybe, Func>) {
            static_assert(std::is_same_v<Maybe, std::invoke_result_t<Func>>,
                          "Maybe::operator| (\"otherwise\") passed a function that does not return Maybe<T>.");
            if (!isSet()) { return func(); }
            else { return *this; }
        } else {
            static_assert(!std::is_same_v<Func, Func>,
                          "Maybe::operator| (\"otherwise\") passed an incompatible callable.");
        }
    }

    constexpr const_iterator cbegin() const { return isSet() ? const_iterator{this} : end(); }
    constexpr const_iterator cend() const { return const_iterator{this, const_iterator::kEndIterator}; }
    constexpr const_iterator begin() const { return cbegin(); }
    constexpr const_iterator end() const { return cend(); }
    constexpr iterator begin() { return isSet() ? iterator{this} : end(); }
    constexpr iterator end() { return iterator{this, iterator::kEndIterator}; }

  private:
    template <typename Ts> struct StorageHelper
    {
        using type = std::conditional_t<std::is_reference_v<Ts>,
                                        std::reference_wrapper<std::remove_reference_t<Ts>>,
                                        Ts>;

        using base = TaggedUnion<Nothing, type>;

        template <typename Base>
        inline static auto& value(Base &v)
        {
            if constexpr (std::is_reference_v<Ts>) { return v.template get<type>().get(); }
            else { return v.template get<type>(); }
        }
    };

    template <typename Ts> struct Storage : StorageHelper<Ts>::base
    {
        using value_type = Ts;
        using storage_type = typename StorageHelper<Ts>::type;
        using Super = typename StorageHelper<Ts>::base;
        using Super::Super;
        inline Ts &value() { return StorageHelper<Ts>::value(*this); }
        inline Ts const &value() const { return StorageHelper<Ts>::value(*this); }
        inline void update(Ts const &v) { Super::set(storage_type{v}); }
        inline void update(std::remove_reference_t<Ts> &&v) { Super::set(storage_type{std::move(v)}); }
    };

    Storage<mutable_type> _store{};

  public:
    template <bool Const>
    struct BasicIterator
    {
        static constexpr const bool kValueIterator = false;
        static constexpr const bool kEndIterator = true;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using difference_type = Maybe::difference_type;
        using reference = const_t<T, Const>;
        using pointer = typename std::conditional<Const, T const *, T *>::type;

        // maintain is_trivially_copy_constructible by defining the Iterator->ConstIterator constructor as a template.
        template <bool OtherConst, typename = typename std::enable_if<Const || !OtherConst>::type>
        BasicIterator(const BasicIterator<OtherConst> &other) : parent_(other.parent_), end_(other.end_) {}

        explicit BasicIterator(const_ptr_t<Maybe, Const> parent) : parent_(parent), end_(kValueIterator) {}
        BasicIterator(const_ptr_t<Maybe, Const> parent, bool end) : parent_(parent), end_(end) {}

        reference operator*() const { return parent_->GetValue(); }

        BasicIterator& operator++() { end_ = true; return *this; }
        BasicIterator operator++(int) { auto copy = *this; operator++(); return copy; }
        BasicIterator& operator--() { end_ = false; return *this; }
        BasicIterator operator--(int) { auto copy = *this; operator--(); return copy; }

        bool operator==(BasicIterator const &rhs) const { return parent_ == rhs.parent_ && end_ == rhs.end_; }
        bool operator!=(BasicIterator const &rhs) const { return !(rhs == *this); }

      private:
        const_ptr_t<Maybe, Const> parent_;
        bool end_;
    };
};

template <typename T> Maybe(T const &) -> Maybe<T>;
template <typename T> Maybe(T &&) -> Maybe<T>;

/**
 * Constructs and "promotes" an object to a Maybe<T>.
 * @tparam T The type of Maybe.
 * @tparam Args The types for one of T's constructors. (deducible)
 * @param args The arguments for one of T's constructors.
 * @return The Maybe.
 */
template <typename T, typename... Args>
static Maybe<T> AsMaybe(Args && ...args)
{
    return Maybe<T>(T(std::forward<Args>(args)...));
}

}} // namespace ax::core
