//
// Created by Martin Miralles-Cordal on 7/27/19.
//

#pragma once

#ifndef _WIN32
#include <sys/param.h>
#endif

#if defined(__APPLE__)
    #define AX_APPLE 1
    #include <TargetConditionals.h>
    #if TARGET_OS_IPHONE
        #define AX_IOS 1
        #if TARGET_OS_EMBEDDED
            #define AX_IOS_DEVICE 1
        #elif TARGET_IPHONE_SIMULATOR
            #define AX_IOS_SIM 1
        #endif
    #else
        #define AX_MACOSX 1
    #endif
#elif defined(_WIN32)
    #define AX_WINDOWS 1
    #if defined(WINDOWS_RUNTIME)
        #define AX_WINRT 1
    #else
        #define AX_WINDOWS_DESKTOP 1
        #if defined(_WIN64)
            #define AX_WIN64 1
        #else
            #define AX_WIN32 1
        #endif
    #endif

#elif defined(__unix__)
    #define AX_UNIX 1
    #if defined(__ANDROID__)
        #define AX_ANDROID 1
    #elif defined(BSD) || defined(_SYSTYPE_BSD)
        #define AX_BSD 1
        #if defined(__FreeBSD__)
            #define AX_FREEBSD 1
        #elif defined(__NetBSD__)
            #define AX_NETBSD 1
        #elif defined(__OpenBSD__)
            #define AX_OPENBSD 1
        #elif defined(__DragonFly__)
            #define AX_DRAGONFLY_BSD 1
        #endif
    #elif defined(__linux__)
        #define AX_LINUX 1
    #endif
#else
    #define AX_OTHER 1
#endif
