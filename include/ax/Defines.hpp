//
// Created by Martin Miralles-Cordal on 9/29/19.
//

#pragma once

#include <ax/Platform.hpp>

#define synchronized(mutex) if (std::lock_guard<decltype(mutex)> _guard##mutex{mutex}; true)

#define AX_TAG_TYPE(name, constant) struct name { explicit name() = default; }; constexpr name constant{}
