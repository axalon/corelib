//
// Created by Martin Miralles-Cordal on 8/4/19.
//

#include <ax/task/Thread.hpp>

#include <ax/Platform.hpp>

#include <array>
#include <atomic>
#include <thread>

namespace ax {
inline namespace task {

namespace {

struct ThreadInfo
{
    std::string name;
    int id = 0;
    std::thread::native_handle_type native{};
    bool nameSetByUser = false;
};

std::atomic_int s_threadCount{0};
thread_local std::unique_ptr<ThreadInfo> tl_currentThread;

#if defined(AX_UNIX)
    #define CURRENT_THREAD pthread_self()
#else
    #define CURRENT_THREAD std::thread::native_handle_type{}
#endif

#if defined(AX_LINUX) || defined(AX_APPLE) || defined(AX_NETBSD)
std::string GetOSThreadName(std::thread::native_handle_type pthread)
{
    std::array<char, 64> buf{};
    pthread_getname_np(pthread, buf.data(), buf.size());
    return std::string{buf.data()};
}
std::string GetOSThreadName() { return GetOSThreadName(pthread_self()); }
#else
std::string GetOSThreadName(std::thread::native_handle_type) { return ""; }
std::string GetOSThreadName() { return ""; }
#endif


#if defined(AX_FREEBSD) || defined(AX_OPENBSD)
    #define PTHREAD_SETNAME pthread_set_name_np
#elif defined(AX_LINUX) || defined(AX_NETBSD) || defined(AX_APPLE)
    #define PTHREAD_SETNAME pthread_setname_np
#endif

#if defined(AX_LINUX) || defined(AX_NETBSD) || defined(AX_FREEBSD) || defined(AX_OPENBSD)
void SetOSThreadName(const char *name) { PTHREAD_SETNAME(pthread_self(), name); }
#elif defined(AX_APPLE)
void SetOSThreadName(const char *name) { pthread_setname_np(name); }
#else
void SetOSThreadName(const char *) {}
#endif

#if defined(AX_LINUX) || defined(AX_NETBSD) || defined(AX_FREEBSD) || defined(AX_OPENBSD)
void SetOSThreadName(std::thread::native_handle_type thr, const char *name) { PTHREAD_SETNAME(thr, name); }
#else
void SetOSThreadName(std::thread::native_handle_type, const char *) {}
#endif

[[maybe_unused]] thread_local bool tl_nameSetByUser = false;

void SetThreadName(ThreadInfo &info, std::string name)
{
    info.nameSetByUser = true;
    info.name = std::move(name);
    if (info.native)
    {
        SetOSThreadName(info.native, info.name.c_str());
    }
}

std::string GetThreadName(ThreadInfo &info)
{
    if (!info.nameSetByUser && info.native) {
        info.name = GetOSThreadName(info.native);
    }

    return info.name;
}

} // anonymous namespace

class Thread::Impl
{
  public:
    Impl(char const *name, Thread *)
            : thread_()
            , info_{name ?: "", s_threadCount++, thread_.native_handle(), false}
    {}

    void start(std::function<void()> body)
    {
        thread_ = std::thread{[info = info_, body = std::move(body)]() {
            tl_currentThread = std::make_unique<ThreadInfo>(info);
            tl_currentThread->native = CURRENT_THREAD;
            body();
        }};
        info_.native = thread_.native_handle();
    }

    ~Impl() { if (thread_.joinable()) { thread_.join(); } }

    [[nodiscard]] inline std::string name() const { return info_.name; }
    void setName(std::string name)
    {
        SetThreadName(info_, std::move(name));
    }

  private:
    std::thread thread_;
    ThreadInfo info_;
};

Thread::Thread(char const *name) : impl_{std::make_unique<Impl>(name, this)} {}
Thread::Thread(std::unique_ptr<ax::task::Thread::Impl> &&impl) : impl_(std::move(impl)) {}

void Thread::start(std::function<void()> body) { impl_->start(std::move(body)); }
std::string Thread::name() const { return impl_->name(); }
void Thread::setName(std::string name) { impl_->setName(std::move(name)); }

}} // namespace ax::task