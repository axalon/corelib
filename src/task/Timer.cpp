//
// Created by Martin Miralles-Cordal on 8/24/19.
//

#include <ax/task/Timer.hpp>

namespace ax {
inline namespace task {

struct Timer::Impl : public std::enable_shared_from_this<Impl>
{
    Impl(Function func, Clock::time_point end, std::chrono::microseconds interval, bool repeating)
            : func_(std::move(func)), end_(end), interval_(interval), repeating_(repeating) {}

    Function func_;
    Clock::time_point end_;
    std::chrono::microseconds interval_;
    const bool repeating_;
    std::atomic_bool cancelled_{false};

    Clock::time_point scheduledTime() const { return end_; }
    explicit operator bool() const { return func_ != nullptr; }
    void cancel() { if (!cancelled_.exchange(true)) { end_ = Clock::time_point{}; } }
    inline void runIfSet() { if (func_) { func_(cancelled_); } }
    void clear() { func_ = nullptr; }

    void operator()()
    {
        runIfSet();
        if (repeating_ && !cancelled_) {
            end_ = Clock::now() + interval_;
        } else {
            invalidate(false);
        }
    }

    void invalidate(bool shouldExecute = false)
    {
        cancel();
        if (shouldExecute) { runIfSet(); }
        clear();
    }
};

std::shared_ptr<Timer::Impl> Timer::InitImpl(ax::Timer::Function func, Timer::Clock::time_point end,
                                             std::chrono::microseconds interval, bool repeating)
{
    return std::make_shared<Timer::Impl>(func, end, interval, repeating);
}

Timer::Clock::time_point Timer::scheduledTime() const { return impl_ ? impl_->scheduledTime() : Clock::time_point{}; }
Timer::operator bool() const { return impl_ ? static_cast<bool>(*impl_) : false; }
bool Timer::ready() const { return readyAt(Clock::now()); }
bool Timer::readyAt(Clock::time_point time) const { return scheduledTime() <= time; }
void Timer::cancel() { if (impl_) { impl_->cancel(); } }
void Timer::invalidate(bool shouldExecute) { if (impl_) { impl_->invalidate(shouldExecute); } }
void Timer::operator()() { if (impl_) { (*impl_)(); } }

}} // namespace ax::task
