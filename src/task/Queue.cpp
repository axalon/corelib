//
// Created by Martin Miralles-Cordal on 8/12/19.
//

#include <ax/task/Queue.hpp>

namespace ax {
inline namespace task {

bool Queue::Worker::canWork() const
{
    return Running() && pool_.get().hasJobs();
}

void Queue::Worker::tryToWork()
{
    pool_.get().queue_.tryPop() >> [](const auto &task) { task(); };
}

void Queue::Worker::operator()()
{
    std::unique_lock<std::mutex> pauseLock{mutex_};

    punchClock_.get().punchIn();

    while (pool_.get().valid()) {
        pool_.get().cond_.wait(pauseLock, [this] { return pool_.get().invalid() || canWork(); });
        tryToWork();
    }

    while (!pool_.get().queue_.empty()) { tryToWork(); }

    punchClock_.get().punchOut();
}


void Queue::Worker::reassign(Queue &newPool)
{
    std::lock_guard<std::mutex> workerLock{mutex_};
    pool_ = newPool;
}

}} // namespace ax::task
