//
// Created by Martin Miralles-Cordal on 8/22/19.
//

#include <ax/TypeTraits.hpp>

#include <map>
#include <string>
#include <vector>

static_assert(ax::is_iterable<std::vector<int>>::value, "std::vector<int> should be iterable.");
static_assert(ax::is_iterable<std::string>::value, "std::string should be iterable.");
static_assert(ax::is_iterable<std::map<std::string, std::vector<std::string>>>::value,
              "std::map<std::string, std::vector<std::string>> should be iterable.");
static_assert(!ax::is_iterable<void>::value, "void should not be iterable.");
static_assert(!ax::is_iterable<int>::value, "int should not be iterable.");
static_assert(ax::is_iterable<int(&)[5]>::value, "int[] should be iterable.");
