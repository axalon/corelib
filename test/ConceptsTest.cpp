//
// Created by Martin Miralles-Cordal on 8/27/19.
//

#include <ax/Concepts.hpp>
#include <ax/core/PropertySet.hpp>

#include <catch2/catch.hpp>

#include <memory>
#include <vector>

using namespace ax;

TEST_CASE("Concepts: Iterator matches correctly", "[Concepts]")
{
    STATIC_REQUIRE(concept::Iterator<std::vector<int>::iterator>);
    STATIC_REQUIRE(concept::Iterator<int*>);
    STATIC_REQUIRE_FALSE(concept::Iterator<std::shared_ptr<int>>);
    STATIC_REQUIRE_FALSE(concept::Iterator<int>);
}

class IntSet
{
  public:
    [[nodiscard]] std::string className() const { return ""; }
    [[nodiscard]] int operator[](std::string_view) const { return 1; }
    [[nodiscard]] Maybe<int> getProperty(std::string_view) const { return Nothing{}; }
    [[nodiscard]] bool hasProperty(std::string_view) const { return false; }
    [[nodiscard]] std::vector<std::string> getAllPropertyNames() const { return {}; }
};

TEST_CASE("Concepts: PropertySet implementable", "[PropertySet][Concepts]")
{
    STATIC_REQUIRE(concept::PropertySet<IntSet, int>);
    STATIC_REQUIRE_FALSE(concept::MutablePropertySet<IntSet, int>);
}