//
// Created by Martin Miralles-Cordal on 10/5/19.
//

#include <ax/task/TimerQueue.hpp>

#include <future>

#include <catch2/catch.hpp>
#include <fmt/format.h>

namespace Timing {

using Clock = std::chrono::high_resolution_clock;
static thread_local Clock::time_point ms_previous;
double now() {
    static auto start = Clock::now();
    return std::chrono::duration<double, std::milli>(Clock::now() - start)
            .count();
}

void sleep(unsigned ms) {
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

} // namespace Timing

TEST_CASE("ax::TimerQueue test")
{
    std::atomic_bool done_{false};
    std::atomic_uint ct{0};
    ax::TimerQueue q;
    std::thread t{[&] { while (!done_) { q.tryWaitForNextTimer(); } while(!q.empty()) { q.tryWaitForNextTimer(); } }};

    // Create timer with ID 1
    q.add(10000, [&ct, start = Timing::now()](bool cancelled) mutable {
        fmt::print("ID 1: cancelled={}, Elapsed {:4.2f}ms\n", cancelled, Timing::now() - start);
        ++ct;
    });

    // Create Timer with ID 2
    q.add(10001, [&ct, start = Timing::now()](bool cancelled) mutable {
        fmt::print("ID 2: cancelled={}, Elapsed {:4.2f}ms\n", cancelled, Timing::now() - start);
        ++ct;
    });

    // Should cancel timers with ID 1 and 2
    q.cancelAll();

    // Create timer with ID 3
    q.add(1000, [&ct, start = Timing::now()](bool cancelled) mutable {
        fmt::print("ID 3: cancelled={}, Elapsed {:4.2f}ms\n", cancelled, Timing::now() - start);
        ++ct;
    });

    // Create timer with ID 4
    auto id = q.add(2000, [&ct, start = Timing::now()](bool cancelled) mutable {
        fmt::print("ID 4: cancelled={}, Elapsed {:4.2f}ms\n", cancelled, Timing::now() - start);
        ++ct;
    });

    // Cancel timer with ID 4
    auto ret = q.cancel(id);
    assert(ret == 1);

    // Give just enough time to execute timer with ID 3 before destroying the
    // TimerQueue
    Timing::sleep(1500);

    // At this point, when destroying TimerQueue, the timer with ID 4 is still
    // pending and will be cancelled implicitly by the destructor
    done_ = true;
    t.join();
    REQUIRE(ct == 4);
}