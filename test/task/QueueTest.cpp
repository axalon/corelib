//
// Created by Martin Miralles-Cordal on 8/3/19.
//

#include <ax/task/Queue.hpp>
#include <ax/task/ThreadPool.hpp>
#include <ax/Algorithms.hpp>

#include <catch2/catch.hpp>
#include <iostream>

using namespace ax;
using namespace std::literals;

TEST_CASE("Make a queue", "[Task][TaskQueue]")
{
    std::atomic_int counter{0};
    const int taskCount = 20000;
    task::ThreadPool<4> q{};
    {
        auto work = std::mem_fn(static_cast<int(std::atomic_int::*)()>(&std::atomic_int::operator++));
        repeat<taskCount>([&] { q.dispatch(work, counter); });
    }
    q.start();
    while(q.hasJobs());
    REQUIRE(counter == taskCount);
}

TEST_CASE("Pause and resume queue", "[Task][TaskQueue]")
{
    std::atomic_int counter{0};
    const int taskCount = 20000;
    task::ThreadPool<4> q{};
    {
        auto work = std::mem_fn(static_cast<int(std::atomic_int::*)()>(&std::atomic_int::operator++));
        repeat<taskCount>([&] { q.dispatch(work, counter); });
    }
    q.start();
    std::this_thread::sleep_for(3ms);
    q.pause();
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    std::this_thread::sleep_for(50ms);
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    q.resume();
    std::this_thread::sleep_for(3ms);
    q.pause();
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    std::this_thread::sleep_for(50ms);
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    q.resume();
    std::this_thread::sleep_for(3ms);
    q.pause();
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    std::this_thread::sleep_for(50ms);
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    q.resume();
    std::this_thread::sleep_for(3ms);
    q.pause();
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    std::this_thread::sleep_for(50ms);
    std::cout << "counter: " << counter << "/" << taskCount << std::endl;
    q.resume();
    while(q.hasJobs());
    REQUIRE(counter == taskCount);
}
