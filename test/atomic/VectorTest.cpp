//
// Created by Martin Miralles-Cordal on 7/30/19.
//

#include <ax/atomic/Vector.hpp>

#include <catch2/catch.hpp>

#include <string>

using namespace ax;

TEST_CASE("Vector can be initialized", "[Atomic][AtomicVector]")
{
    SECTION("Default")
    {
        atomic::Vector<std::string> vstrs;
        REQUIRE(vstrs.empty());
    }

    SECTION("Initializer list")
    {
        atomic::Vector<std::string> vstrs{"Hello", "World!"};
        REQUIRE(vstrs.size() == 2);
    }

    SECTION("Value initialization")
    {
        atomic::Vector<std::string> vstrs{15, "Oh!"};
        REQUIRE(vstrs.size() == 15);
        for (auto &str : vstrs) { REQUIRE(str == "Oh!"); }
    }

    SECTION("Copy initialization")
    {
        atomic::Vector<std::string> vstrs1{"Hello", "World"};
        atomic::Vector<std::string> vstrs2{vstrs1};
    }
}

TEST_CASE("Vector can be accessed", "[Atomic][AtomicVector]")
{
    atomic::Vector<std::string> vstrs{2, "Hey"};
    REQUIRE(vstrs.front() == "Hey");
    REQUIRE(vstrs.back() == "Hey");
    for (std::size_t i = 0; i < vstrs.size(); ++i)
    {
        auto v = vstrs[i];
        REQUIRE(vstrs[i] == "Hey");
        REQUIRE(v == vstrs[i]);
    }
}

TEST_CASE("Vector can be modified", "[Atomic][AtomicVector]")
{
    atomic::Vector<std::string> vstrs;
    REQUIRE(vstrs.empty());

    SECTION("Vector can push_back")
    {
        vstrs.push_back("Hello");
        REQUIRE(vstrs.size() == 1);
        REQUIRE(vstrs[0] == "Hello");
        vstrs.push_back("World");
        REQUIRE(vstrs.size() == 2);
        REQUIRE(vstrs[1] == "World");
        vstrs.push_back("!");
        REQUIRE(vstrs.size() == 3);
        REQUIRE(vstrs[2] == "!");
    }

    SECTION("Vector can erase")
    {
        vstrs.push_back("Hello");
        vstrs.push_back("of");
        vstrs.push_back("World");
        REQUIRE(vstrs.size() == 3);
        vstrs.remove("of");
        REQUIRE(vstrs.size() == 2);
        REQUIRE(vstrs[1] == "World");
    }

    SECTION("Vector can be assigned")
    {
        vstrs = {"Hello", "World", "!"};
        REQUIRE(vstrs.size() == 3);
    }
}

TEST_CASE("Vector can be iterated through", "[Atomic][AtomicVector]")
{
    const int count = 15;
    atomic::Vector<std::string> vstrs{count, "AAA"};

    SECTION("When mutable")
    {
        int iterations = 0;
        for (auto &str : vstrs)
        {
            REQUIRE(str == "AAA");
            ++iterations;
        }
        REQUIRE(iterations == count);
    }

    SECTION("When const")
    {
        auto const &cvstrs = vstrs;
        int iterations = 0;
        for (auto &str : cvstrs)
        {
            REQUIRE(str == "AAA");
            ++iterations;
        }
        REQUIRE(iterations == count);
    }
}
