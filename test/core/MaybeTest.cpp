//
// Created by Martin Miralles-Cordal on 7/30/19.
//

#include <catch2/catch.hpp>

#include <ax/core/Maybe.hpp>

#include <iostream>

using namespace ax;
using namespace ax::core;

namespace {

Maybe<double> SquareRoot(int x)
{
    if (x >= 0) {
        return std::sqrt(x);
    } else {
        return Nothing{};
    }
}

template <typename T>
std::string PrintMaybe(const Maybe<T> &m)
{
    if (m) {
        return std::string{"Just "} + std::to_string(*m);
    } else {
        return "Nothing";
    }
}

template <typename T>
std::ostream &operator<<(std::ostream &os, Maybe<T> const &m)
{
    return os << PrintMaybe(m);
}

}

TEST_CASE("Monadic operators", "[Maybe]")
{
  using namespace std::literals;
 
  int wv = 44; 
  Maybe<int&> w{wv};
  REQUIRE(w.isSet() == true);
  REQUIRE(w.get() == 44);
 
  std::cout << "wv = " << wv << '\n';
  std::cout << "w.Get() = " << w.get() << '\n';
  std::cout << "=====================\n";
 
  wv = 10; 
  REQUIRE(w.isSet() == true);
  REQUIRE(w.get() == 10);
                                                                                                                        
  std::cout << "wv = " << wv << '\n';
  std::cout << "w.Get() = " << w.get() << '\n';
  std::cout << "=====================\n";
 
  const Maybe<int> x;
  const Maybe<int> y{4};
  const Maybe<int> z{-4};
  REQUIRE(x.isSet() == false);
  REQUIRE(y.isSet() == true);
  REQUIRE(y.get() == 4); 
  REQUIRE(z.isSet() == true);
  REQUIRE(z.get() == -4);
 
  std::cout << "x: " << x << '\n';
  std::cout << "y: " << y << '\n';
  std::cout << "z: " << z << '\n';
  std::cout << "=====================\n";
 
  constexpr static const auto fallback = 5643.0;
  const auto xsequence1 = x >> SquareRoot;
  std::cout << "x >> SquareRoot: " << xsequence1 << '\n';
  REQUIRE(xsequence1.isSet() == false);
  const auto xsequence2 = x >> SquareRoot | []() { return AsMaybe<double>(fallback); };
  std::cout << "x >> SquareRoot | <otherwise fallback>: " << xsequence2 << '\n';
  REQUIRE(xsequence2.isSet() == true);
  REQUIRE(xsequence2.get() == fallback);
 
  const auto ysequence1 = y >> SquareRoot;
  std::cout << "y >> SquareRoot: " << ysequence1 << '\n';
  REQUIRE(ysequence1.isSet() == true);
  REQUIRE(ysequence1.get() == 2.0);
  const auto ysequence2 = y >> SquareRoot | []() { return AsMaybe<double>(fallback); };
  std::cout << "y >> SquareRoot | <otherwise fallback>: " << ysequence2 << '\n';
  REQUIRE(ysequence2.isSet() == true);
  REQUIRE(ysequence2.get() == 2.0);
  const auto ysequence3 = y >> SquareRoot >> [](double d) -> Maybe<double>{ return d * 2; };
  std::cout << "y >> SquareRoot >> <double lambda>: " << ysequence3 << '\n';
  REQUIRE(ysequence3.isSet() == true);
  REQUIRE(ysequence3.get() == 4.0);

  const auto zsequence = z >> SquareRoot;
  std::cout << "z >> SquareRoot: " << zsequence << std::endl;
  REQUIRE(zsequence.isSet() == false);
}
