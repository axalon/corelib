//
// Created by Martin Miralles-Cordal on 8/13/19.
//

#include <catch2/catch.hpp>

#include <ax/core/ThreadLocalCache.hpp>
#include <ax/Algorithms.hpp>
#include <ax/task/ThreadPool.hpp>

using namespace std::literals;

namespace {

struct Result
{
    static std::atomic_int instantiations;
    Result(std::pair<int, std::string> pair) : input(pair.first), output(pair.second) { ++instantiations; }
    int input;
    std::string output;
};

std::atomic_int Result::instantiations{0};

}

TEST_CASE("Thread local cache", "[ThreadLocalCache]")
{
    const int uniqueKeys = 10;
    const int threads = 20;
    ax::ThreadLocalCache<int, Result> cache;
    {
        ax::task::ThreadPool<threads> threadPool;
        ax::repeat<10000>([&] {
            for (int i = 0; i < uniqueKeys; ++i) {
                threadPool.dispatch([&] {
                    cache.emplace(i, std::make_pair(i, std::to_string(i)));
                });
            }
        });

        threadPool.start();
        while(threadPool.hasJobs()) { std::this_thread::sleep_for(10ms); }
    }
    REQUIRE(Result::instantiations <= uniqueKeys * threads);
}
