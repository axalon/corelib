//
// Created by Martin Miralles-Cordal on 8/4/19.
//

#include <ax/core/Signal.hpp>

#include <catch2/catch.hpp>

using namespace std::literals;
#define REQUIRE_MSG(expr, msg) do { INFO(msg); REQUIRE(expr); } while(0)

TEST_CASE("Signal notifies", "[Signal]")
{
    SECTION("Signal<> (no data)")
    {
        ax::Signal<> voidSig{};
        bool signalled = false;
        auto token = voidSig.connect([&] { signalled = true; });
        voidSig.emit();
        REQUIRE(signalled);
    }

    SECTION("Signal<int>")
    {
        ax::Signal<int> intSig;
        int value = 0;
        auto token = intSig.connect([&](int v) { value = v; });
        intSig.emit(20);
        REQUIRE(value == 20);
        intSig.emit(40);
        REQUIRE(value == 40);
        token.disconnect();
        intSig.emit(80);
        REQUIRE(value == 40);
    }
}

TEST_CASE("Signal token works as intended", "[Signal]")
{
    ax::Signal<int> intSig;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
    SECTION("Token not retained")
    {
        intSig.connect([](int) { REQUIRE_MSG(false, "Signal received without retaining the token."); });
        intSig.emit(0xDEADBEEF);
    }
#pragma GCC diagnostic pop

    SECTION("Token retained")
    {
        int value = 0;
        {
            decltype(intSig)::Token t; // token is default-constructible
            {
                // check that token is move-assignable
                t = intSig.connect([&](int v) { value = v; });
            }

            intSig.emit(10);
            REQUIRE_MSG(value == 10, "Did not receive emitted signal while connected.");

            SECTION("Token reassigned")
            {
                int value_sqd = 0;
                t = intSig.connect([&](int v) { value_sqd = v*v; });
                intSig.emit(12);
                REQUIRE_MSG(value == 10,
                            "Value updater still received signal after its token was reassigned.");
                REQUIRE_MSG(value_sqd == 144,
                            "Value^2 updater did not receive a signal while its token is alive and in scope.");
            }
        }

        SECTION("Token goes out of scope")
        {
            intSig.emit(20);
            REQUIRE_MSG(value == 10, "Received emitted signal after token went out of scope.");
        }
    }
}
